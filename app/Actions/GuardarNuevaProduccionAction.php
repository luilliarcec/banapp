<?php

namespace App\Actions;

use App\Models\Insumo;
use App\Models\Parametro;
use App\Models\Produccion;
use App\Rules\StockRule;
use Illuminate\Support\Facades\DB;
use Lorisleiva\Actions\Action;

/**
 * @method static mixed run(array $data)
 */
class GuardarNuevaProduccionAction extends Action
{
    private Produccion $produccion;
    private Parametro $parametros;

    public function initialized(Produccion $produccion)
    {
        $this->produccion = $produccion;
        $this->parametros = Parametro::query()->firstOrFail();
    }

    protected function prepareForValidation()
    {
        $costo_produccion = $this->obtenerCostoProduccion();
        $total_produccion = $this->obtenerTotalDeProduccion($costo_produccion);
        $precio_caja_venta = $this->obtenerPrecioCajaVenta($total_produccion);

        return $this->fill([
            'existencia' => $this->get('caja_totales'),
            'precio_base_caja' => $this->parametros->precio_base_caja,
            'total_manos_caja' => $this->parametros->total_manos_caja,
            'porcentaje_utilidad' => $this->parametros->porcentaje_utilidad,
            'costo_produccion' => $costo_produccion,
            'total_produccion' => $total_produccion,
            'precio_caja_venta' => $precio_caja_venta,
        ]);
    }

    public function rules()
    {
        return [
            'lote_id' => 'required|exists:lotes,id',
            'variedad_id' => 'required|exists:variedades,id',
            'precio_base_caja' => 'required', // Interno
            'total_manos_caja' => 'required', // Interno
            'porcentaje_utilidad' => 'required', // Interno
            'costo_adicional_produccion' => 'nullable|numeric',
            'fecha_produccion' => 'required|date|before_or_equal:now',
            'total_manos' => 'required|integer|min:1',
            'rechazos' => 'required|integer|min:0',
            'caja_totales' => 'required|integer|min:1',
            'existencia' => 'required|same:caja_totales', // Interno
            'precio_caja_venta' => 'required', // Interno
            'costo_produccion' => 'required', // Interno
            'total_produccion' => 'required', // Interno
            'observacion' => 'required|max:800',

            'detalle' => 'required|array',
            'detalle.*.insumo_id' => 'required|exists:insumos,id',
            'detalle.*.cantidad' => [
                'required',
                'integer',
                new StockRule(
                    Insumo::query(),
                    'existencia',
                    'insumo_id',
                    $this->get('detalle')
                )
            ],
            'detalle.*.costo_referencial' => 'required|numeric',
        ];
    }

    public function handle()
    {
        DB::transaction(function () {
            $data = $this->validated();

            $this->produccion->fill($data)->save();

            foreach (data_get($data, 'detalle') as $detalle) {
                $this->produccion->insumos()->attach($detalle['insumo_id'],
                    array_intersect_key($detalle,
                        array_flip(['cantidad', 'costo_referencial'])
                    )
                );
            }
        });

        return $this->produccion;
    }

    private function obtenerCostoProduccion(): float
    {
        $insumos = collect($this->get('detalle'));

        return
            $this->get('costo_adicional_produccion') +
            $insumos->sum(
                function ($insumo) {
                    return $insumo['cantidad'] * $insumo['costo_referencial'];
                }
            );
    }

    private function obtenerTotalDeProduccion(float $costo_produccion): float
    {
        return $costo_produccion + $costo_produccion *
            ($this->parametros->porcentaje_utilidad / 100);
    }

    private function obtenerPrecioCajaVenta(float $total_produccion): float
    {
        if ($this->get('caja_totales') == 0)
            return 0;

        $precio_caja_venta = $total_produccion / $this->get('caja_totales');

        return ($precio_caja_venta > $this->parametros->precio_base_caja) ?
            $precio_caja_venta : $this->parametros->precio_base_caja;
    }
}
