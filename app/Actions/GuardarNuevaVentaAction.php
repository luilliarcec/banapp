<?php

namespace App\Actions;

use App\Models\Produccion;
use App\Models\Venta;
use App\Rules\StockRule;
use Illuminate\Support\Facades\DB;
use Lorisleiva\Actions\Action;

/**
 * @method static mixed run(array $data)
 */
class GuardarNuevaVentaAction extends Action
{
    private Venta $venta;

    public function initialized(Venta $venta)
    {
        $this->venta = $venta;
    }

    public function rules()
    {
        return [
            'cliente_id' => 'required|integer|exists:clientes,id',
            'fecha_venta' => 'required|date|before_or_equal:now',

            'detalle' => 'required|array',
            'detalle.*.produccion_id' => 'required|integer|exists:producciones,id',
            'detalle.*.cantidad' => [
                'required',
                'integer',
                'min:1',
                new StockRule(
                    Produccion::class,
                    'existencia',
                    'produccion_id',
                    $this->get('detalle')
                )
            ],
            'detalle.*.precio' => 'required|numeric|min:0.01',
        ];
    }

    public function handle()
    {
        DB::transaction(function () {
            $data = $this->validated();

            $this->venta->fill($data)->save();

            foreach (data_get($data, 'detalle') as $detalle) {
                $this->venta->cajas()->attach($detalle['produccion_id'], [
                    'cantidad' => $detalle['cantidad'],
                    'precio' => $detalle['precio']
                ]);
            }
        });

        return $this->venta;
    }
}
