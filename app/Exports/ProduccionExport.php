<?php

namespace App\Exports;

use App\Models\Produccion;
use Illuminate\Contracts\Queue\ShouldQueue;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithStyles;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

class ProduccionExport implements FromCollection, WithHeadings, WithMapping, WithStyles, ShouldAutoSize, ShouldQueue
{
    use Exportable;

    public function headings(): array
    {
        return [
            'ID',
            'Lote',
            'Variedad',
            'Precio base de caja',
            'Total de manos por caja',
            'Fecha de producción',
            'Total de manos producidas',
            'Total de manos rechazadas',
            'Total de cajas producidas',
            'Existencia de cajas',
            'Precio de venta',
            'Costo de producción',
            'Total de producción con utilidad',
        ];
    }

    public function map($row): array
    {
        return [
            $row->getKey(),
            $row->lote->nombre,
            $row->variedad->nombre,
            $row->precio_base_caja,
            $row->total_manos_caja,
            $row->dateFormat('fecha_produccion'),
            $row->total_manos,
            $row->rechazos,
            $row->caja_totales,
            $row->existencia,
            $row->moneyFormat('precio_caja_venta'),
            $row->moneyFormat('costo_produccion'),
            $row->moneyFormat('total_produccion'),
        ];
    }

    public function styles(Worksheet $sheet)
    {
        return [
            // Style the first row as bold text.
            1 => ['font' => ['bold' => true]],
        ];
    }

    public function collection()
    {
        return Produccion::with('lote', 'variedad')->applyFilters()->get();
    }
}
