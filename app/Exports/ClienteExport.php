<?php

namespace App\Exports;

use App\Models\Cliente;
use Illuminate\Contracts\Queue\ShouldQueue;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithStyles;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

class ClienteExport implements FromCollection, WithHeadings, WithMapping, WithStyles, ShouldAutoSize, ShouldQueue
{
    use Exportable;

    public function headings(): array
    {
        return [
            'ID',
            'Nombre',
            'Cédula',
            'Email',
            'Teléfono',
            'Dirección',
            'Fecha de creación',
        ];
    }

    public function map($row): array
    {
        return [
            $row->getKey(),
            $row->nombre,
            $row->cedula,
            $row->email,
            $row->telefono,
            $row->direccion,
            $row->created_at->isoFormat('ll'),
        ];
    }

    public function styles(Worksheet $sheet)
    {
        return [
            // Style the first row as bold text.
            1 => ['font' => ['bold' => true]],
        ];
    }

    public function collection()
    {
        return Cliente::query()->applyFilters()->get();
    }
}
