<?php

namespace App\Exports;

use App\Models\Venta;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithStyles;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

class VentasExport implements FromCollection, WithHeadings, WithMapping, WithStyles, ShouldAutoSize, ShouldQueue
{
    use Exportable;

    private Collection $collection;
    private Request $request;

    public function __construct()
    {
        $this->collection = Venta::with(['vendedor', 'cliente', 'cajas'])
            ->applyFilters()
            ->get();

        $this->request = request();
    }

    public function headings(): array
    {
        $desde = $this->request->has('desde')
            ? sprintf('DESDE %s', $this->request->desde)
            : null;

        $hasta = $this->request->has('hasta')
            ? sprintf('HASTA %s', $this->request->hasta)
            : null;

        return [
            ['REPORTE DE VENTAS ', $desde, $hasta],
            [],
            ['SUBTOTAL: ', $this->collection->sum('sub_total')],
            ['TOTAL IVA: ', $this->collection->sum('iva')],
            ['TOTAL: ', $this->collection->sum('total')],
            [],
            [
                'ID',
                'Cliente',
                'Vendedor',
                'Fecha de venta',
                'Subtotal',
                'Porcentaje IVA',
                'IVA',
                'Total',
            ]
        ];
    }

    public function map($row): array
    {
        return [
            $row->getKey(),
            $row->cliente->nombre,
            $row->vendedor->nombre,
            $row->dateFormat('fecha_venta'),
            $row->moneyFormat('subtotal'),
            $row->porcentaje_iva,
            $row->moneyFormat('iva'),
            $row->moneyFormat('total'),
        ];
    }

    public function styles(Worksheet $sheet): array
    {
        return [
            1 => ['font' => ['bold' => true]],
            7 => ['font' => ['bold' => true]],
        ];
    }

    public function collection()
    {
        return $this->collection;
    }
}
