<?php

namespace App\Exports;

use App\Models\Insumo;
use Illuminate\Contracts\Queue\ShouldQueue;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithStyles;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

class InsumoExport implements FromCollection, WithColumnFormatting, WithHeadings, WithMapping, WithStyles, ShouldAutoSize, ShouldQueue
{
    use Exportable;

    public function headings(): array
    {
        return [
            'ID',
            'Tipo de insumo',
            'Insumo',
            'Descripción',
            'Existencia',
            'Costo referencial',
            'Costo total',
            'Fecha de creación',
        ];
    }

    public function map($row): array
    {
        return [
            $row->getKey(),
            $row->tipo->nombre,
            $row->nombre,
            $row->descripcion,
            $row->existencia,
            $row->costo_referencial,
            $row->costo_referencial * $row->existencia,
            $row->created_at->isoFormat('ll'),
        ];
    }

    public function styles(Worksheet $sheet): array
    {
        return [
            // Style the first row as bold text.
            1 => ['font' => ['bold' => true]],
        ];
    }

    public function columnFormats(): array
    {
        return [
            'F' => NumberFormat::FORMAT_CURRENCY_USD_SIMPLE,
            'G' => NumberFormat::FORMAT_CURRENCY_USD_SIMPLE,
        ];
    }

    public function collection()
    {
        return Insumo::with(['tipo'])->applyFilters()->get();
    }
}
