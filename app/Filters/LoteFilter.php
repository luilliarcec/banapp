<?php

namespace App\Filters;

use Luilliarcec\LaravelQueryFilter\AbstractFilter;
use Luilliarcec\LaravelQueryFilter\Rules\SortableColumn;

class LoteFilter extends AbstractFilter
{
    public function rules(): array
    {
        return [
            'search' => 'filled',
            'order' => [new SortableColumn(['nombre', 'cantidad', 'unidad', 'descripcion', 'created_at'])],
        ];
    }

    public function search($query, $value)
    {
        return $query->where('nombre', 'like', "%{$value}%");
    }
}
