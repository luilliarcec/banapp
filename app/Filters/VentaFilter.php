<?php

namespace App\Filters;

use App\Queries\VentaQuery;
use Luilliarcec\LaravelQueryFilter\AbstractFilter;
use Luilliarcec\LaravelQueryFilter\Rules\SortableColumn;

class VentaFilter extends AbstractFilter
{
    public function rules(): array
    {
        return [
            'search' => 'filled',
            'desde' => 'date',
            'hasta' => 'date|after:desde',
            'order' => [
                new SortableColumn([
                    'fecha_venta',
                ])
            ],
        ];
    }

    public function search(VentaQuery $query, $value)
    {
        return $query
            ->whereHas('cliente', function ($query) use ($value) {
                return $query->where('nombre', 'like', "%{$value}%")
                    ->orWhere('cedula', 'like', $value);
            });
    }

    public function desde(VentaQuery $query, $value)
    {
        return $query
            ->whereDate('fecha_venta', '>=', $value);
    }

    public function hasta(VentaQuery $query, $value)
    {
        return $query
            ->whereDate('fecha_venta', '<=', $value);
    }
}
