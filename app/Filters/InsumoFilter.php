<?php

namespace App\Filters;

use App\Queries\InsumoQuery;
use Luilliarcec\LaravelQueryFilter\AbstractFilter;
use Luilliarcec\LaravelQueryFilter\Rules\SortableColumn;

class InsumoFilter extends AbstractFilter
{
    public function rules(): array
    {
        return [
            'search' => 'filled',
            'desde' => 'date',
            'hasta' => 'date|after:desde',
            'order' => [new SortableColumn(['nombre', 'descripcion', 'existencia', 'costo_referencial', 'created_at'])],
        ];
    }

    public function search($query, $value)
    {
        return $query->where('nombre', 'like', "%{$value}%")
            ->orWhereHas('tipo', function ($query) use ($value) {
                $query->where('nombre', 'like', "%{$value}%");
            });
    }

    public function desde(InsumoQuery $query, $value)
    {
        return $query
            ->whereDate('created_at', '>=', $value);
    }

    public function hasta(InsumoQuery $query, $value)
    {
        return $query
            ->whereDate('created_at', '<=', $value);
    }
}
