<?php

namespace App\Filters;

use App\Queries\ProduccionQuery;
use Luilliarcec\LaravelQueryFilter\AbstractFilter;
use Luilliarcec\LaravelQueryFilter\Rules\SortableColumn;

class ProduccionFilter extends AbstractFilter
{
    public function rules(): array
    {
        return [
            'search' => 'filled',
            'desde' => 'date',
            'hasta' => 'date|after:desde',
            'order' => [
                new SortableColumn([
                    'precio_base_caja',
                    'total_manos_caja',
                    'fecha_produccion',
                    'total_manos',
                    'rechazos',
                    'caja_totales',
                    'existencia',
                    'precio_caja_venta',
                    'costo_produccion',
                    'total_produccion',
                ])
            ],
        ];
    }

    public function search(ProduccionQuery $query, $value)
    {
        return $query->where(function ($query) use ($value) {
            $query
                ->whereHas('lote', function ($query) use ($value) {
                    return $query->where('nombre', 'like', "%{$value}%");
                })
                ->orWhereHas('variedad', function ($query) use ($value) {
                    return $query->where('nombre', 'like', "%{$value}%");
                });
        });
    }

    public function desde(ProduccionQuery $query, $value)
    {
        return $query
            ->whereDate('fecha_produccion', '>=', $value);
    }

    public function hasta(ProduccionQuery $query, $value)
    {
        return $query
            ->whereDate('fecha_produccion', '<=', $value);
    }
}
