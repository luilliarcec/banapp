<?php

namespace App\Filters;

use Luilliarcec\LaravelQueryFilter\AbstractFilter;
use Luilliarcec\LaravelQueryFilter\Rules\SortableColumn;

class ClienteFilter extends AbstractFilter
{
    public function rules(): array
    {
        return [
            'search' => 'filled',
            'order' => [new SortableColumn(['nombre', 'cedula', 'email', 'created_at'])],
        ];
    }

    public function search($query, $value)
    {
        return $query->where('nombre', 'like', "%{$value}%")
            ->orWhere('cedula', 'like', "{$value}%")
            ->orWhere('email', 'like', "{$value}%")
            ->orWhere('telefono', 'like', "{$value}%");
    }
}
