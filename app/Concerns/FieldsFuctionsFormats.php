<?php

namespace App\Concerns;

trait FieldsFuctionsFormats
{
    public function moneyFormat($field, $prefix = '')
    {
        return $prefix . number_format($this->{$field}, 2, ',', '.');
    }

    public function dateFormat($field, $isoFormat = 'll')
    {
        return is_null($this->{$field}) ? 'Sin fecha' : $this->{$field}->isoFormat($isoFormat);
    }
}
