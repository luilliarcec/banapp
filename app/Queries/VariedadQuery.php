<?php

namespace App\Queries;

use Luilliarcec\LaravelQueryFilter\QueryBuilder;

class VariedadQuery extends QueryBuilder
{
    public function listadoCargadoConProduccion()
    {
        return $this
            ->with('producciones', function ($query) {
                $query->with('lote:id,nombre')->where('existencia', '>', 0);
            })
            ->has('producciones', '>', 0);
    }
}
