<?php

namespace App\View\Components;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cache;
use Illuminate\View\Component;

class Form extends Component
{
    /** @var string Form method */
    public string $method;

    /** @var Model|null Bind model */
    public ?Model $model;

    /**
     * Form constructor.
     *
     * @param string $method
     * @param Model|null $model
     */
    public function __construct(string $method = 'post', Model $model = null)
    {
        $this->method = $method;
        $this->model = $model;
    }

    /**
     * Form destruct
     */
    public function __destruct()
    {
        Cache::forget('b-model');
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        if ($this->model) {
            Cache::put('b-model', $this->model);
        }

        return view('components.form');
    }
}
