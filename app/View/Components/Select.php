<?php

namespace App\View\Components;

use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Cache;
use Illuminate\View\Component;

class Select extends Component
{
    /** @var string Select name */
    public string $name;

    /** @var array Select options */
    public array $options;

    /** @var string Select label */
    public string $label;

    /** @var string|array|null Select value */
    public $value;

    /** @var string|null Empty option */
    public ?string $empty;

    /** @var string|null Select help */
    public ?string $help;

    /** @var bool Required indicator */
    public bool $required;

    /** @var bool Multiple indicator */
    public bool $multiple;

    /**
     * Select constructor.
     *
     * @param string $name
     * @param array $options
     * @param string|null $label
     * @param string|null $value
     * @param string|null $empty
     * @param string|null $help
     * @param string|null $required
     * @param string|null $multiple
     */
    public function __construct(string $name, array $options = [], string $label = null, $value = null, string $empty = null, string $help = null, string $required = null, string $multiple = null)
    {
        $this->name = $name;
        $this->options = $options;
        $this->label = $this->label($label, $this->cleanName());
        $this->value = $this->value($value, $name);
        $this->empty = $empty;
        $this->help = $help;
        $this->required = !is_null($required);
        $this->multiple = !is_null($multiple);
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        return view('components.select');
    }

    /**
     * Returns css style if it has errors
     *
     * @param bool $hasErrors
     * @return string
     */
    public function styles(bool $hasErrors)
    {
        return $hasErrors ? 'form-control is-invalid' : 'form-control';
    }

    /**
     * Return the name without brackets for a multi select
     *
     * @return string
     */
    public function cleanName()
    {
        return str_replace('[]', '', $this->name);
    }

    /**
     * Returns selected attribute
     *
     * @param string $key
     * @param bool $errors
     * @return string
     */
    public function isSelected(string $key, bool $errors = false)
    {
        if ($this->multiple) {
            if ($errors) {
                if (in_array($key, old($this->cleanName(), []))) {
                    return 'selected';
                }
            }

            if ($this->value instanceof Collection) {
                if ($this->value->contains($key)) {
                    return 'selected';
                }
            }

            return '';
        } else {
            return $key == $this->value ? 'selected' : '';
        }
    }

    /**
     * Obtains the value of the input, which can be passed as an attribute to the component
     * or obtained from the form model, both checked in the old function.
     *
     * @param ?string $value
     * @param string $name
     * @return string|null
     */
    private function value(?string $value, string $name)
    {
        $model = Cache::get('b-model');

        if (is_object($model)) {
            return old($name, $model->{$this->cleanName()});
        }

        return old($name, $value);
    }

    /**
     * Gets the label for the input, which can be passed as an
     * attribute or obtained from the input name
     *
     * @param ?string $label
     * @param string $name
     * @return string
     */
    private function label(?string $label, string $name)
    {
        return $label ? $label : ucfirst(str_replace('_', ' ', $name));
    }
}
