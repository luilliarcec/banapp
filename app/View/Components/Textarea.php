<?php

namespace App\View\Components;

use Illuminate\Support\Facades\Cache;
use Illuminate\View\Component;

class Textarea extends Component
{
    /** @var string Textarea name */
    public string $name;

    /** @var string Textarea label */
    public string $label;

    /** @var string|null Textarea value */
    public ?string $value;

    /** @var string|null Textarea help */
    public ?string $help;

    /** @var bool Required indicator */
    public bool $required;

    /**
     * Textarea constructor.
     *
     * @param string $name
     * @param string|null $label
     * @param string|null $value
     * @param string|null $required
     * @param string|null $help
     */
    public function __construct(string $name, string $label = null, string $value = null, string $help = null, string $required = null)
    {
        $this->name = $name;
        $this->label = $this->label($label, $name);
        $this->value = $this->value($value, $name);
        $this->required = !is_null($required);
        $this->help = $help;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        return view('components.textarea');
    }

    /**
     * Returns css style if it has errors
     *
     * @param bool $hasErrors
     * @return string
     */
    public function styles(bool $hasErrors)
    {
        return $hasErrors ? 'form-control is-invalid' : 'form-control';
    }

    /**
     * Obtains the value of the input, which can be passed as an attribute to the component
     * or obtained from the form model, both checked in the old function.
     *
     * @param ?string $value
     * @param string $name
     * @return string|null
     */
    private function value(?string $value, string $name)
    {
        $model = Cache::get('b-model');

        if (is_object($model)) {
            return old($name, $model->{$name});
        }

        return old($name, $value);
    }

    /**
     * Gets the label for the input, which can be passed as an
     * attribute or obtained from the input name
     *
     * @param ?string $label
     * @param string $name
     * @return string
     */
    private function label(?string $label, string $name)
    {
        return $label ? $label : ucfirst(str_replace('_', ' ', $name));
    }
}
