<?php

namespace App\View\Components;

use Illuminate\Support\Facades\Cache;
use Illuminate\View\Component;

class Input extends Component
{
    /** @var string[] Valid types */
    private const TYPES = [
        'text', 'password', 'number', 'url', 'search', 'email', 'hidden', 'range', 'file', 'color',
        'date', 'month', 'week', 'time', 'datetime-local'
    ];

    /** @var string Input type */
    public string $type;

    /** @var string Input name */
    public string $name;

    /** @var string Input label */
    public string $label;

    /** @var string|null Input value */
    public ?string $value;

    /** @var string|null Input help */
    public ?string $help;

    /** @var bool Required indicator */
    public bool $required;

    /**
     * Input constructor.
     *
     * @param string $type
     * @param string $name
     * @param string|null $label
     * @param string|null $value
     * @param string|null $help
     * @param string|null $required
     */
    public function __construct(string $type, string $name, string $label = null, string $value = null, string $help = null, string $required = null)
    {
        $this->type = $type;
        $this->name = $name;
        $this->label = $this->label($label, $name);
        $this->value = $this->value($value, $name);
        $this->required = !is_null($required);
        $this->help = $help;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        return view('components.input');
    }

    /**
     * Returns css style if it has errors
     *
     * @param bool $hasErrors
     * @return string
     */
    public function styles(bool $hasErrors)
    {
        return $hasErrors ? 'form-control is-invalid' : 'form-control';
    }

    /**
     * Obtains the value of the input, which can be passed as an attribute to the component
     * or obtained from the form model, both checked in the old function.
     *
     * @param ?string $value
     * @param string $name
     * @return string|null
     */
    private function value(?string $value, string $name)
    {
        $model = Cache::get('b-model');

        if (is_object($model)) {
            if (!in_array($name, $model->getHidden())) {
                return old($name, $model->{$name});
            }
        }

        return old($name, $value);
    }

    /**
     * Gets the label for the input, which can be passed as an
     * attribute or obtained from the input name
     *
     * @param ?string $label
     * @param string $name
     * @return string
     */
    private function label(?string $label, string $name)
    {
        return $label ? $label : ucfirst(str_replace('_', ' ', $name));
    }
}
