<?php

namespace App\View\Components;

use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Cache;
use Illuminate\View\Component;
use Luilliarcec\LaravelQueryFilter\Overrides\LengthAwarePaginator;

class Th extends Component
{
    /** @var string */
    public string $name;

    /** @var LengthAwarePaginator|Collection */
    private $sort;

    /**
     * Th constructor.
     *
     * @param string $name
     */
    public function __construct(string $name)
    {
        $this->name = $name;
        $this->sort = Cache::get('b-collection');
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        return view('components.th');
    }

    /**
     * Render th link
     *
     * @return string
     */
    public function links()
    {
        if ($this->sort instanceof LengthAwarePaginator)
            return $this->sort->sortUrl($this->name);

        return '';
    }

    /**
     * Render th styles
     *
     * @return string
     */
    public function styles()
    {
        if ($this->sort instanceof LengthAwarePaginator)
            return $this->sort->classes($this->name);

        return '';
    }
}
