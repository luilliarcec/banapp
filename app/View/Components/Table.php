<?php

namespace App\View\Components;

use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Cache;
use Illuminate\View\Component;

class Table extends Component
{
    /** @var LengthAwarePaginator|Collection */
    public $collection;

    /** @var boolean */
    public bool $isResponsive;

    /**
     * Table constructor.
     *
     * @param $collection
     * @param bool $isResponsive
     */
    public function __construct($collection, bool $isResponsive = true)
    {
        $this->collection = $collection;
        $this->isResponsive = $isResponsive;
    }

    /**
     * Table destructor.
     */
    public function __destruct()
    {
        Cache::forget('b-collection');
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        Cache::put('b-collection', $this->collection);

        return view('components.table');
    }

    /**
     * Render responsive table styles
     *
     * @return string
     */
    public function responsiveStyles()
    {
        return $this->isResponsive ? 'table-responsive' : '';
    }
}
