<?php

namespace App\Models;

use App\Queries\InsumoQuery;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Insumo extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'insumos';

    protected $fillable = [
        'tipo_id', 'nombre', 'descripcion', 'existencia', 'costo_referencial',
    ];

    public function tipo()
    {
        return $this->belongsTo(Tipo::class, 'tipo_id');
    }

    public function newEloquentBuilder($query)
    {
        return new InsumoQuery($query);
    }
}
