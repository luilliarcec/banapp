<?php

namespace App\Models\Relations;

use App\Models\Insumo;
use Illuminate\Database\Eloquent\Relations\Pivot;

class InsumoProduccion extends Pivot
{
    protected $table = 'insumo_produccion';

    public $incrementing = true;

    protected static function booted()
    {
        static::creating(function ($pivot) {
            Insumo::query()->find($pivot->insumo_id)
                ->decrement('existencia', $pivot->cantidad);
        });
    }
}
