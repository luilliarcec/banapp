<?php

namespace App\Models\Relations;

use App\Models\Produccion;
use Illuminate\Database\Eloquent\Relations\Pivot;

class ProduccionVenta extends Pivot
{
    protected $table = 'produccion_venta';

    public $incrementing = true;

    protected static function booted()
    {
        static::creating(function ($pivot) {
            Produccion::query()->find($pivot->produccion_id)
                ->decrement('existencia', $pivot->cantidad);
        });
    }
}
