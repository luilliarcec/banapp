<?php

namespace App\Models;

use App\Concerns\FieldsFuctionsFormats;
use App\Models\Relations\InsumoProduccion;
use App\Queries\ProduccionQuery;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Produccion extends Model
{
    use HasFactory, FieldsFuctionsFormats;

    protected $table = 'producciones';

    protected $fillable = [
        'lote_id',
        'variedad_id',
        'precio_base_caja',
        'total_manos_caja',
        'porcentaje_utilidad',
        'fecha_produccion',
        'total_manos',
        'rechazos',
        'caja_totales',
        'existencia',
        'precio_caja_venta',
        'costo_produccion',
        'total_produccion',
        'observacion',
    ];

    protected $casts = [
        'fecha_produccion' => 'datetime'
    ];

    public function lote()
    {
        return $this->belongsTo(Lote::class, 'lote_id')->withTrashed();
    }

    public function variedad()
    {
        return $this->belongsTo(Variedad::class, 'variedad_id')->withTrashed();
    }

    public function insumos()
    {
        return $this->belongsToMany(Insumo::class, 'insumo_produccion', 'produccion_id', 'insumo_id')
            ->using(InsumoProduccion::class)
            ->withPivot('cantidad', 'costo_referencial');
    }

    public function newEloquentBuilder($query)
    {
        return new ProduccionQuery($query);
    }
}
