<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Empresa extends Model
{
    use HasFactory;

    protected $table = 'empresas';

    protected $fillable = [
        'razon_social', 'nombre_comercial', 'ruc', 'telefono',
        'direccion', 'representante_legal', 'dni_representante_legal'
    ];
}
