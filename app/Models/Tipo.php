<?php

namespace App\Models;

use App\Queries\TipoQuery;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Tipo extends Model
{
    use HasFactory;

    protected $table = 'tipos';

    protected $fillable = ['nombre', 'descripcion'];

    public function insumos()
    {
        return $this->hasMany(Insumo::class, 'tipo_id');
    }

    public function newEloquentBuilder($query)
    {
        return new TipoQuery($query);
    }
}
