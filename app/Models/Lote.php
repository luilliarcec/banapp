<?php

namespace App\Models;

use App\Queries\LoteQuery;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Lote extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'lotes';

    protected $fillable = ['nombre', 'cantidad', 'unidad', 'descripcion'];

    public function producciones()
    {
        return $this->hasMany(Produccion::class, 'lote_id');
    }

    public function newEloquentBuilder($query)
    {
        return new LoteQuery($query);
    }
}
