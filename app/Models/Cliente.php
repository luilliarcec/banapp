<?php

namespace App\Models;

use App\Queries\ClienteQuery;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Cliente extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'clientes';

    protected $fillable = [
        'nombre', 'cedula', 'email', 'telefono', 'direccion',
    ];

    public function newEloquentBuilder($query)
    {
        return new ClienteQuery($query);
    }
}
