<?php

namespace App\Models;

use App\Queries\VariedadQuery;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Variedad extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'variedades';

    protected $fillable = ['nombre', 'descripcion'];

    public function producciones()
    {
        return $this->hasMany(Produccion::class, 'variedad_id');
    }

    public function newEloquentBuilder($query)
    {
        return new  VariedadQuery($query);
    }
}
