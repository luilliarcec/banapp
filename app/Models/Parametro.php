<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Parametro extends Model
{
    use HasFactory;

    protected $table = 'parametros';

    protected $fillable = [
        'precio_base_caja', 'total_manos_caja', 'porcentaje_utilidad', 'porcentaje_iva'
    ];
}
