<?php

namespace App\Models;

use App\Concerns\FieldsFuctionsFormats;
use App\Models\Relations\ProduccionVenta;
use App\Queries\VentaQuery;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Venta extends Model
{
    use HasFactory, FieldsFuctionsFormats;

    protected $table = 'ventas';

    protected $fillable = [
        'usuario_id',
        'cliente_id',
        'fecha_venta',
        'porcentaje_iva',
    ];

    protected $casts = [
        'fecha_venta' => 'datetime'
    ];

    protected static function booted()
    {
        static::creating(function ($venta) {
            $venta->vendedor()->associate(auth()->user());
            $venta->porcentaje_iva = Parametro::query()->first()->porcentaje_iva;
        });
    }

    public function getSubTotalAttribute()
    {
        if (!$this->relationLoaded('cajas')) return 0;

        $value = $this->cajas->sum(function ($item) {
            return $item->pivot->cantidad * $item->pivot->precio;
        });

        return round($value, 2);
    }

    public function getIvaAttribute()
    {
        if (!$this->relationLoaded('cajas')) return 0;

        $value = $this->subtotal * ($this->porcentaje_iva / 100);

        return round($value, 2);
    }

    public function getTotalAttribute()
    {
        if (!$this->relationLoaded('cajas')) return 0;

        $value = $this->subtotal + $this->iva;

        return round($value, 2);
    }

    public function vendedor()
    {
        return $this->belongsTo(User::class, 'vendedor_id')->withTrashed();
    }

    public function cliente()
    {
        return $this->belongsTo(Cliente::class, 'cliente_id')->withTrashed();
    }

    public function cajas()
    {
        return $this->belongsToMany(Produccion::class, 'produccion_venta', 'venta_id', 'produccion_id')
            ->using(ProduccionVenta::class)
            ->withPivot('cantidad', 'precio');
    }

    public function newEloquentBuilder($query)
    {
        return new VentaQuery($query);
    }
}
