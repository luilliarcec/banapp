<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
    public $timestamps = false;

    protected $with = ['childrens'];

    protected static function booted()
    {
        static::addGlobalScope('withoutHidden', function (Builder $builder) {
            $builder->where('hidden', 0);
        });
    }

    protected $fillable = ['parent_id', 'title', 'link', 'icon', 'ability', 'scope', 'hidden'];

    public function parent()
    {
        return $this->belongsTo(Menu::class, 'parent_id');
    }

    public function childrens()
    {
        return $this->hasMany(Menu::class, 'parent_id');
    }
}
