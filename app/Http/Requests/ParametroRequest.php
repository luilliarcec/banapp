<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ParametroRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'precio_base_caja' => 'required|numeric|min:0.01',
            'total_manos_caja' => 'required|integer|min:1',
            'porcentaje_utilidad' => 'required|numeric|min:0.01',
            'porcentaje_iva' => 'required|numeric|min:0.01',
        ];
    }
}
