<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EmpresaRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'razon_social' => 'required|max:255|unique:empresas',
            'nombre_comercial' => 'required|max:255',
            'ruc' => 'required|digits:13|ecuador:ruc',
            'telefono' => 'nullable|digits_between:9,20',
            'direccion' => 'nullable|max:255',
            'representante_legal' => 'required|max:255',
            'dni_representante_legal' => 'required|digits:10',
        ];
    }
}
