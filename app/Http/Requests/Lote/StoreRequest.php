<?php

namespace App\Http\Requests\Lote;

use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nombre' => 'required|max:70|unique:lotes',
            'cantidad' => 'required|numeric',
            'unidad' => 'required|max:70',
            'descripcion' => 'nullable|max:255',
        ];
    }
}
