<?php

namespace App\Http\Requests\Insumo;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'tipo_id' => 'required|exists:tipos,id',
            'nombre' => [
                'required', 'max:70',
                Rule::unique('insumos')->ignore($this->route('insumo'))
            ],
            'descripcion' => 'nullable|max:255',
            'existencia' => 'required|integer',
            'costo_referencial' => 'required|numeric',
        ];
    }
}
