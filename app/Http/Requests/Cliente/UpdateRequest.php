<?php

namespace App\Http\Requests\Cliente;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nombre' => 'required|max:255',
            'cedula' => ['required', 'ecuador:all_identifications', Rule::unique('clientes')->ignore($this->route('cliente'))],
            'email' => ['required', 'max:255', 'email', Rule::unique('clientes')->ignore($this->route('cliente'))],
            'telefono' => 'nullable|max:20',
            'direccion' => 'nullable|max:255',
        ];
    }
}
