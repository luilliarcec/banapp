<?php

namespace App\Http\Requests\Tipo;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nombre' => [
                'required', 'max:70',
                Rule::unique('tipos')->ignore($this->route('tipo'))
            ],
            'descripcion' => 'nullable|max:255',
        ];
    }
}
