<?php

namespace App\Http\Middleware;

use App\Models\Menu;
use Closure;
use Illuminate\Http\Request;

class ResponseSidebarMenu
{
    public function handle(Request $request, Closure $next)
    {
        view()->share('sidebar',
            Menu::query()->whereNull('parent_id')->get()
        );

        return $next($request);
    }
}
