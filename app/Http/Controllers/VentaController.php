<?php

namespace App\Http\Controllers;

use App\Actions\GuardarNuevaVentaAction;
use App\Exports\VentasExport;
use App\Http\Resources\VariedadResource;
use App\Models\Cliente;
use App\Models\Parametro;
use App\Models\Variedad;
use App\Models\Venta;
use Illuminate\Http\Request;
use LaravelDaily\Invoices\Classes\InvoiceItem;
use LaravelDaily\Invoices\Classes\Party;
use LaravelDaily\Invoices\Invoice;

class VentaController extends Controller
{
    public function __construct()
    {
        $this->middleware('can:Listar ventas')->only('index');
        $this->middleware('can:Crear ventas')->only('create', 'store');
    }

    public function index()
    {
        return view('app.venta.index', [
            'data' => Venta::with(['vendedor', 'cliente', 'cajas'])->applyFilters()->paginate()
        ]);
    }

    public function create()
    {
        return view('app.venta.create', [
            'parametros' => Parametro::query()->firstOrFail()->toJson(),
            'clientes' => Cliente::query()->get(['id', 'nombre', 'cedula'])->toJson(),
            'variedades' => VariedadResource::collection(Variedad::query()->listadoCargadoConProduccion()->get())->toJson()
        ]);
    }

    public function store(Request $request)
    {
        GuardarNuevaVentaAction::run($request->all());

        return response()->json(['message' => 'Venta registrada existosamente']);
    }

    public function show(Venta $venta)
    {
        $venta->load(['cajas.variedad', 'cliente', 'vendedor']);

        return view('app.venta.show', compact('venta'));
    }

    public function informe()
    {
        return (new VentasExport())->download(
            sprintf('Informe de ventas %s.xlsx', now())
        );
    }

    public function factura(Venta $venta)
    {
        $venta->load(['cajas.variedad', 'cliente', 'vendedor']);

        $items = array();

        foreach ($venta->cajas as $caja) {
            array_push(
                $items,
                (new InvoiceItem())
                    ->title($caja->variedad->descripcion)
                    ->pricePerUnit($caja->pivot->precio)
                    ->quantity($caja->pivot->cantidad)
            );
        }

        return Invoice::make()
            ->name('Bananapp')
            ->sequence($venta->id)
            ->date($venta->fecha_venta)
            ->seller(new Party([
                'name' => $venta->vendedor->nombre,
            ]))
            ->buyer(new Party([
                'name' => $venta->cliente->nombre,
                'address' => $venta->cliente->direccion,
                'custom_fields' => [
                    'RUC/CI' => $venta->cliente->cedula,
                ]
            ]))
            ->addItems($items)
            ->taxRate($venta->porcentaje_iva)
            ->stream();
    }
}
