<?php

namespace App\Http\Controllers;

use App\Http\Requests\EmpresaRequest;
use App\Models\Empresa;

class EmpresaController extends Controller
{
    public function __construct()
    {
        $this->middleware('can:Ver empresa')->only('show');
        $this->middleware('can:Actualizar empresa')->only('edit', 'update');
    }

    public function show()
    {
        return view('app.empresas.show', [
            'empresa' => Empresa::query()->firstOrNew(),
        ]);
    }

    public function edit()
    {
        return view('app.empresas.edit', [
            'empresa' => Empresa::query()->firstOrNew(),
        ]);
    }

    public function update(EmpresaRequest $request)
    {
        Empresa::query()->firstOrNew()->fill($request->validated())->save();

        return redirect()->back()->with(['message' => 'Registro actualizado con éxito']);
    }
}
