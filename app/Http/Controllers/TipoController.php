<?php

namespace App\Http\Controllers;

use App\Http\Requests\Tipo\StoreRequest;
use App\Http\Requests\Tipo\UpdateRequest;
use App\Models\Tipo;

class TipoController extends Controller
{
    public function __construct()
    {
        $this->middleware('can:Listar tipos')->only('index');
        $this->middleware('can:Crear tipos')->only('create', 'store');
        $this->middleware('can:Editar tipos')->only('edit', 'update');
    }

    public function index()
    {
        return view('app.tipo.index', [
            'data' => Tipo::query()->applyFilters()->paginate()
        ]);
    }

    public function create()
    {
        return view('app.tipo.create');
    }

    public function store(StoreRequest $request)
    {
        Tipo::query()->create($request->validated());

        return redirect()->back()->with(['message' => 'Registro guardado con éxito']);
    }

    public function edit(Tipo $tipo)
    {
        return view('app.tipo.edit', compact('tipo'));
    }

    public function update(UpdateRequest $request, Tipo $tipo)
    {
        $tipo->update($request->validated());

        return redirect()->back()->with(['message' => 'Registro actualizado con éxito']);
    }
}
