<?php

namespace App\Http\Controllers;

use App\Http\Requests\ParametroRequest;
use App\Models\Parametro;

class ParametroController extends Controller
{
    public function __construct()
    {
        $this->middleware('can:Ver parámetros')->only('show');
        $this->middleware('can:Actualizar parámetros')->only('edit', 'update');
    }

    public function show()
    {
        return view('app.parametros.show', [
            'parametro' => Parametro::query()->firstOrNew(),
        ]);
    }

    public function edit()
    {
        return view('app.parametros.edit', [
            'parametro' => Parametro::query()->firstOrNew(),
        ]);
    }

    public function update(ParametroRequest $request)
    {
        Parametro::query()->firstOrNew()->fill($request->validated())->save();

        return redirect()->back()->with(['message' => 'Registro actualizado con éxito']);
    }
}
