<?php

namespace App\Http\Controllers;

use App\Http\Requests\Lote\{StoreRequest, UpdateRequest};
use App\Models\Lote;

class LoteController extends Controller
{
    public function __construct()
    {
        $this->middleware('can:Listar lotes')->only('index');
        $this->middleware('can:Crear lotes')->only('create', 'store');
        $this->middleware('can:Editar lotes')->only('edit', 'update');
        $this->middleware('can:Eliminar lotes')->only('destroy');
        $this->middleware('can:Restaurar lotes')->only('restore');
    }

    public function index()
    {
        return view('app.lotes.index', [
            'data' => Lote::query()
                ->onlyTrashedIf(request()->routeIs('lotes.trash'))
                ->applyFilters()
                ->paginate()
        ]);
    }

    public function create()
    {
        return view('app.lotes.create');
    }

    public function store(StoreRequest $request)
    {
        Lote::query()->create($request->validated());

        return redirect()->back()->with(['message' => 'Registro guardado con éxito']);
    }

    public function edit(Lote $lote)
    {
        return view('app.lotes.edit', compact('lote'));
    }

    public function update(UpdateRequest $request, Lote $lote)
    {
        $lote->update($request->validated());

        return redirect()->back()->with(['message' => 'Registro actualizado con éxito']);
    }

    public function destroy(Lote $lote)
    {
        $lote->delete();

        return response()->json(['message' => 'Registro eliminado con éxito']);
    }

    public function restore($id)
    {
        Lote::onlyTrashed()->findOrFail($id)->restore();

        return response()->json(['message' => 'Registro restaurado con éxito']);
    }
}
