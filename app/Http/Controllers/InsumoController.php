<?php

namespace App\Http\Controllers;

use App\Exports\InsumoExport;
use App\Http\Requests\Insumo\StoreRequest;
use App\Http\Requests\Insumo\UpdateRequest;
use App\Models\Insumo;
use App\Models\Tipo;

class InsumoController extends Controller
{
    public function __construct()
    {
        $this->middleware('can:Listar insumos')->only('index');
        $this->middleware('can:Crear insumos')->only('create', 'store');
        $this->middleware('can:Editar insumos')->only('edit', 'update');
        $this->middleware('can:Eliminar insumos')->only('destroy');
        $this->middleware('can:Restaurar insumos')->only('restore');
    }

    public function index()
    {
        return view('app.insumos.index', [
            'data' => Insumo::with('tipo')->onlyTrashedIf(request()->routeIs('insumos.trash'))
                ->applyFilters()->paginate()
        ]);
    }

    public function create()
    {
        return view('app.insumos.create', [
            'tipos' => Tipo::all(['id', 'nombre'])->pluck('nombre', 'id')->toArray()
        ]);
    }

    public function store(StoreRequest $request)
    {
        Insumo::query()->create($request->validated());

        return redirect()->back()->with(['message' => 'Registro guardado con éxito']);
    }

    public function edit(Insumo $insumo)
    {
        return view('app.insumos.edit', [
            'insumo' => $insumo,
            'tipos' => Tipo::all(['id', 'nombre'])->pluck('nombre', 'id')->toArray()
        ]);
    }

    public function update(UpdateRequest $request, Insumo $insumo)
    {
        $insumo->update($request->validated());

        return redirect()->back()->with(['message' => 'Registro actualizado con éxito']);
    }

    public function destroy(Insumo $insumo)
    {
        $insumo->delete();

        return response()->json(['message' => 'Registro eliminado con éxito']);
    }

    public function restore($id)
    {
        Insumo::onlyTrashed()->findOrFail($id)->restore();

        return response()->json(['message' => 'Registro restaurado con éxito']);
    }

    public function informe()
    {
        return (new InsumoExport())->download(
            sprintf('Informe de insumos %s.xlsx', now())
        );
    }
}
