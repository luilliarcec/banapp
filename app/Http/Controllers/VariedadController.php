<?php

namespace App\Http\Controllers;

use App\Http\Requests\Variedad\{StoreRequest, UpdateRequest};
use App\Models\Variedad;

class VariedadController extends Controller
{
    public function __construct()
    {
        $this->middleware('can:Listar variedades')->only('index');
        $this->middleware('can:Crear variedades')->only('create', 'store');
        $this->middleware('can:Editar variedades')->only('edit', 'update');
        $this->middleware('can:Eliminar variedades')->only('destroy');
        $this->middleware('can:Restaurar variedades')->only('restore');
    }

    public function index()
    {
        return view('app.variedades.index', [
            'data' => Variedad::query()
                ->onlyTrashedIf(request()->routeIs('variedades.trash'))
                ->applyFilters()
                ->paginate()
        ]);
    }

    public function create()
    {
        return view('app.variedades.create');
    }

    public function store(StoreRequest $request)
    {
        Variedad::query()->create($request->validated());

        return redirect()->back()->with(['message' => 'Registro guardado con éxito']);
    }

    public function edit(Variedad $variedad)
    {
        return view('app.variedades.edit', compact('variedad'));
    }

    public function update(UpdateRequest $request, Variedad $variedad)
    {
        $variedad->update($request->validated());

        return redirect()->back()->with(['message' => 'Registro actualizado con éxito']);
    }

    public function destroy(Variedad $variedad)
    {
        $variedad->delete();

        return response()->json(['message' => 'Registro eliminado con éxito']);
    }

    public function restore($id)
    {
        Variedad::onlyTrashed()->findOrFail($id)->restore();

        return response()->json(['message' => 'Registro restaurado con éxito']);
    }
}
