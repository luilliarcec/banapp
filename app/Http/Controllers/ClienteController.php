<?php

namespace App\Http\Controllers;

use App\Exports\ClienteExport;
use App\Http\Requests\Cliente\StoreRequest;
use App\Http\Requests\Cliente\UpdateRequest;
use App\Models\Cliente;

class ClienteController extends Controller
{
    public function __construct()
    {
        $this->middleware('can:Listar clientes')->only('index');
        $this->middleware('can:Crear clientes')->only('create', 'store');
        $this->middleware('can:Editar clientes')->only('edit', 'update');
        $this->middleware('can:Eliminar clientes')->only('destroy');
        $this->middleware('can:Restaurar clientes')->only('restore');
    }

    public function index()
    {
        return view('app.clientes.index', [
            'data' => Cliente::query()->onlyTrashedIf(request()->routeIs('clientes.trash'))
                ->applyFilters()->paginate()
        ]);
    }

    public function create()
    {
        return view('app.clientes.create');
    }

    public function store(StoreRequest $request)
    {
        Cliente::query()->create($request->validated());

        return redirect()->back()->with(['message' => 'Registro guardado con éxito']);
    }

    public function edit(Cliente $cliente)
    {
        return view('app.clientes.edit', compact('cliente'));
    }

    public function update(UpdateRequest $request, Cliente $cliente)
    {
        $cliente->update($request->validated());

        return redirect()->back()->with(['message' => 'Registro actualizado con éxito']);
    }

    public function destroy(Cliente $cliente)
    {
        $cliente->delete();

        return response()->json(['message' => 'Registro eliminado con éxito']);
    }

    public function restore($id)
    {
        Cliente::onlyTrashed()->findOrFail($id)->restore();

        return response()->json(['message' => 'Registro restaurado con éxito']);
    }

    public function informe()
    {
        return (new ClienteExport())->download(
            sprintf('Informe de clientes %s.xlsx', now())
        );
    }
}
