<?php

namespace App\Http\Controllers;

use App\Actions\GuardarNuevaProduccionAction;
use App\Exports\ProduccionExport;
use App\Models\Lote;
use App\Models\Parametro;
use App\Models\Produccion;
use App\Models\Insumo;
use App\Models\Variedad;
use Illuminate\Http\Request;

class ProduccionController extends Controller
{
    public function __construct()
    {
        $this->middleware('can:Listar producciones')->only('index');
        $this->middleware('can:Crear producciones')->only('create', 'store');
    }

    public function index()
    {
        return view('app.produccion.index', [
            'data' => Produccion::with('lote', 'variedad')->applyFilters()->paginate()
        ]);
    }

    public function create()
    {
        return view('app.produccion.create', [
            'parametros' => Parametro::query()->firstOrFail()->toJson(),
            'variedades' => Variedad::all(['id', 'nombre'])->toJson(),
            'lotes' => Lote::all(['id', 'nombre'])->toJson(),
            'insumos' => Insumo::all(['id', 'nombre', 'existencia', 'costo_referencial'])->toJson(),
        ]);
    }

    public function store(Request $request)
    {
        GuardarNuevaProduccionAction::run($request->all());

        return response()->json(['message' => 'Producción registrada existosamente']);
    }

    public function show(Produccion $produccion)
    {
        $produccion->load('insumos');

        return view('app.produccion.show', compact('produccion'));
    }

    public function informe()
    {
        return (new ProduccionExport())->download(
            sprintf('Informe de produccion %s.xlsx', now())
        );
    }
}
