<?php

namespace App\Http\Controllers;

use App\Http\Requests\User\StoreRequest;
use App\Http\Requests\User\UpdateRequest;
use App\Models\User;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('can:Listar usuarios')->only('index');
        $this->middleware('can:Crear usuarios')->only('create', 'store');
        $this->middleware('can:Editar usuarios')->only('edit', 'update');
        $this->middleware('can:Eliminar usuarios')->only('destroy');
        $this->middleware('can:Restaurar usuarios')->only('restore');
    }

    public function index()
    {
        return view('app.usuarios.index', [
            'data' => User::query()->onlyTrashedIf(request()->routeIs('usuarios.trash'))
                ->applyFilters()->paginate()
        ]);
    }

    public function create()
    {
        return view('app.usuarios.create', [
            'roles' => Role::all()->pluck('name', 'id')->toArray(),
            'permissions' => Permission::all()->pluck('name', 'id')->toArray(),
        ]);
    }

    public function store(StoreRequest $request)
    {
        DB::transaction(function () use ($request) {
            $usuario = User::query()->create($request->validated());
            $usuario->permissions()->sync($request->input('permissions'));
            $usuario->roles()->sync($request->input('roles'));
        });

        return redirect()->back()->with(['message' => 'Registro guardado con éxito']);
    }

    public function show(User $usuario)
    {
        //
    }

    public function edit(User $usuario)
    {
        return view('app.usuarios.edit', [
            'usuario' => $usuario->load('roles', 'permissions'),
            'roles' => Role::all()->pluck('name', 'id')->toArray(),
            'permissions' => Permission::all()->pluck('name', 'id')->toArray(),
        ]);
    }

    public function update(UpdateRequest $request, User $usuario)
    {
        DB::transaction(function () use ($request, $usuario) {
            $usuario->permissions()->sync($request->input('permissions'));
            $usuario->roles()->sync($request->input('roles'));
            $usuario->update($request->validated());
        });

        return redirect()->back()->with(['message' => 'Registro actualizado con éxito']);
    }

    public function destroy(User $usuario)
    {
        $usuario->delete();

        return response()->json(['message' => 'Registro eliminado con éxito']);
    }

    public function restore($id)
    {
        User::onlyTrashed()->findOrFail($id)->restore();

        return response()->json(['message' => 'Registro restaurado con éxito']);
    }
}
