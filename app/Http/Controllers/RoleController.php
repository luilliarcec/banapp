<?php

namespace App\Http\Controllers;

use App\Http\Requests\Role\StoreRequest;
use App\Http\Requests\Role\UpdateRequest;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class RoleController extends Controller
{
    public function __construct()
    {
        $this->middleware('can:Listar roles')->only('index');
        $this->middleware('can:Crear roles')->only('create', 'store');
        $this->middleware('can:Editar roles')->only('edit', 'update');
        $this->middleware('can:Eliminar roles')->only('destroy');
    }

    public function index()
    {
        return view('app.roles.index', [
            'data' => Role::with('permissions')->paginate()
        ]);
    }

    public function create()
    {
        return view('app.roles.create', [
            'permissions' => Permission::all()->pluck('name', 'id')->toArray(),
        ]);
    }

    public function store(StoreRequest $request)
    {
        DB::transaction(function () use ($request) {
            $role = Role::create($request->validated());
            $role->permissions()->sync($request->input('permissions'));
        });

        return redirect()->back()->with(['message' => 'Registro guardado con éxito']);
    }

    public function edit(Role $role)
    {
        return view('app.roles.edit', [
            'role' => $role,
            'permissions' => Permission::all()->pluck('name', 'id')->toArray(),
        ]);
    }

    public function update(UpdateRequest $request, Role $role)
    {
        DB::transaction(function () use ($request, $role) {
            $role->permissions()->sync($request->input('permissions'));
            $role->update($request->validated());
        });

        return redirect()->back()->with(['message' => 'Registro actualizado con éxito']);
    }

    public function destroy(Role $role)
    {
        $role->delete();

        return response()->json(['message' => 'Registro eliminado con éxito']);
    }
}
