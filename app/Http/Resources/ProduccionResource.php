<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

/** @mixin \App\Models\Produccion */
class ProduccionResource extends JsonResource
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        $mes_anio = sprintf('%s/%s', $this->fecha_produccion->month, $this->fecha_produccion->year);

        return [
            'id' => $this->id,
            'descripcion' => sprintf('%s - %s', $this->lote->nombre, $mes_anio),
            'existencia' => $this->existencia,
            'precio_caja_venta' => $this->precio_caja_venta,
        ];
    }
}
