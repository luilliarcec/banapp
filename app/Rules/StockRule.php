<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Str;

class StockRule implements Rule
{
    private $model;
    private string $field;
    private string $inputNameId;
    private array $items;

    /**
     * StockRule constructor.
     *
     * @param $model string|\Illuminate\Database\Eloquent\Builder
     * @param string $field
     * @param string $inputNameId
     * @param array $items
     */
    public function __construct($model, string $field, string $inputNameId, array $items)
    {
        $this->model = $model;
        $this->field = $field;
        $this->inputNameId = $inputNameId;
        $this->items = $items;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param string $attribute
     * @param mixed $value
     * @return bool
     */
    public function passes($attribute, $value): bool
    {
        if (is_string($this->model)) {
            return $this->model::query()
                ->where($this->field, '>=', $value)
                ->whereKey($this->getModelId($attribute))
                ->exists();
        }

        return $this->model
            ->where($this->field, '>=', $value)
            ->whereKey($this->getModelId($attribute))
            ->exists();
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message(): string
    {
        return 'El campo :attribute supera el stock.';
    }

    /**
     * Get input model id
     *
     * @param $attribute
     * @return mixed
     */
    private function getModelId($attribute)
    {
        $key = $this->getKeyOfItems($attribute);

        return $this->items[$key][$this->inputNameId];
    }

    /**
     * Get the key or index in items array
     *
     * @param $attribute
     * @return string
     */
    private function getKeyOfItems($attribute): string
    {
        return (string)Str::of($attribute)->match('!\d+!');
    }
}
