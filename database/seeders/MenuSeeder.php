<?php

namespace Database\Seeders;

use App\Models\Menu;
use Illuminate\Database\Seeder;

class MenuSeeder extends Seeder
{
    public function run()
    {
        // cib-shopify - ventas
        $this->empresas();
        $this->parametros();
        $this->usuarios();
        $this->roles();
        $this->lotes();
        $this->variedades();
        $this->tipo();
        $this->insumos();
        $this->produccion();
        $this->clientes();
        $this->venta();
    }

    private function empresas()
    {
        $parent = Menu::query()->updateOrCreate(['title' => 'Empresa'], [
            'icon' => 'cil-factory',
        ]);

        Menu::query()->updateOrCreate(['link' => 'empresas.show'], [
            'parent_id' => $parent->getKey(),
            'ability' => 'Ver empresa',
            'title' => 'Información de la empresa',
        ]);

        Menu::query()->updateOrCreate(['link' => 'empresas.edit'], [
            'parent_id' => $parent->getKey(),
            'ability' => 'Actualizar empresa',
            'title' => 'Actualizar información',
        ]);
    }

    private function parametros()
    {
        $parent = Menu::query()->updateOrCreate(['title' => 'Parámetros'], [
            'icon' => 'cil-braille',
        ]);

        Menu::query()->updateOrCreate(['link' => 'parametros.show'], [
            'parent_id' => $parent->getKey(),
            'ability' => 'Ver parámetros',
            'title' => 'Datos de los parámetros',
        ]);

        Menu::query()->updateOrCreate(['link' => 'parametros.edit'], [
            'parent_id' => $parent->getKey(),
            'ability' => 'Actualizar parámetros',
            'title' => 'Actualizar parámetros',
        ]);
    }

    private function usuarios()
    {
        $parent = Menu::query()->updateOrCreate(['title' => 'Usuarios'], [
            'icon' => 'cil-user',
        ]);

        Menu::query()->updateOrCreate(['link' => 'usuarios.index'], [
            'parent_id' => $parent->getKey(),
            'ability' => 'Listar usuarios',
            'title' => 'Listado de usuarios',
        ]);

        Menu::query()->updateOrCreate(['link' => 'usuarios.create'], [
            'parent_id' => $parent->getKey(),
            'ability' => 'Crear usuarios',
            'title' => 'Crear nuevos usuarios',
        ]);

        Menu::query()->updateOrCreate(['link' => 'usuarios.trash'], [
            'parent_id' => $parent->getKey(),
            'ability' => 'Restaurar usuarios',
            'title' => 'Listado de usuarios eliminados',
        ]);
    }

    private function roles()
    {
        $parent = Menu::query()->updateOrCreate(['title' => 'Roles'], [
            'icon' => 'cil-fingerprint',
        ]);

        Menu::query()->updateOrCreate(['link' => 'roles.index'], [
            'parent_id' => $parent->getKey(),
            'ability' => 'Listar roles',
            'title' => 'Listado de roles',
        ]);

        Menu::query()->updateOrCreate(['link' => 'roles.create'], [
            'parent_id' => $parent->getKey(),
            'ability' => 'Crear roles',
            'title' => 'Crear nuevos roles',
        ]);
    }

    private function lotes()
    {
        $parent = Menu::query()->updateOrCreate(['title' => 'Lotes'], [
            'icon' => 'cil-flag-alt',
        ]);

        Menu::query()->updateOrCreate(['link' => 'lotes.index'], [
            'parent_id' => $parent->getKey(),
            'ability' => 'Listar lotes',
            'title' => 'Listado de lotes',
        ]);

        Menu::query()->updateOrCreate(['link' => 'lotes.create'], [
            'parent_id' => $parent->getKey(),
            'ability' => 'Crear lotes',
            'title' => 'Crear nuevos lotes',
        ]);

        Menu::query()->updateOrCreate(['link' => 'lotes.trash'], [
            'parent_id' => $parent->getKey(),
            'ability' => 'Restaurar lotes',
            'title' => 'Listado de lotes eliminados',
        ]);
    }

    private function variedades()
    {
        $parent = Menu::query()->updateOrCreate(['title' => 'Variedades'], [
            'icon' => 'cil-clipboard',
        ]);

        Menu::query()->updateOrCreate(['link' => 'variedades.index'], [
            'parent_id' => $parent->getKey(),
            'ability' => 'Listar variedades',
            'title' => 'Listado de variedades',
        ]);

        Menu::query()->updateOrCreate(['link' => 'variedades.create'], [
            'parent_id' => $parent->getKey(),
            'ability' => 'Crear variedades',
            'title' => 'Crear nuevas variedades',
        ]);

        Menu::query()->updateOrCreate(['link' => 'variedades.trash'], [
            'parent_id' => $parent->getKey(),
            'ability' => 'Restaurar variedades',
            'title' => 'Listado de variedades eliminadas',
        ]);
    }

    private function tipo()
    {
        $parent = Menu::query()->updateOrCreate(['title' => 'Tipos de insumo'], [
            'icon' => 'cil-leaf',
        ]);

        Menu::query()->updateOrCreate(['link' => 'tipos-insumo.index'], [
            'parent_id' => $parent->getKey(),
            'ability' => 'Listar tipos',
            'title' => 'Listado de tipos',
        ]);

        Menu::query()->updateOrCreate(['link' => 'tipos-insumo.create'], [
            'parent_id' => $parent->getKey(),
            'ability' => 'Crear tipos',
            'title' => 'Crear nuevos tipos',
        ]);
    }

    private function insumos()
    {
        $parent = Menu::query()->updateOrCreate(['title' => 'Insumos'], [
            'icon' => 'cil-drop',
        ]);

        Menu::query()->updateOrCreate(['link' => 'insumos.index'], [
            'parent_id' => $parent->getKey(),
            'ability' => 'Listar insumos',
            'title' => 'Listado de insumos',
        ]);

        Menu::query()->updateOrCreate(['link' => 'insumos.create'], [
            'parent_id' => $parent->getKey(),
            'ability' => 'Crear insumos',
            'title' => 'Crear nuevos insumos',
        ]);

        Menu::query()->updateOrCreate(['link' => 'insumos.trash'], [
            'parent_id' => $parent->getKey(),
            'ability' => 'Restaurar insumos',
            'title' => 'Listado de insumos eliminados',
        ]);
    }

    private function produccion()
    {
        $parent = Menu::query()->updateOrCreate(['title' => 'Producción'], [
            'icon' => 'cil-spa',
        ]);

        Menu::query()->updateOrCreate(['link' => 'produccion.index'], [
            'parent_id' => $parent->getKey(),
            'ability' => 'Listar producciones',
            'title' => 'Listado de producciones',
        ]);

        Menu::query()->updateOrCreate(['link' => 'produccion.create'], [
            'parent_id' => $parent->getKey(),
            'ability' => 'Crear producciones',
            'title' => 'Crear nuevas producciones',
        ]);
    }

    private function clientes()
    {
        $parent = Menu::query()->updateOrCreate(['title' => 'Clientes'], [
            'icon' => 'cil-mood-very-good',
        ]);

        Menu::query()->updateOrCreate(['link' => 'clientes.index'], [
            'parent_id' => $parent->getKey(),
            'ability' => 'Listar clientes',
            'title' => 'Listado de clientes',
        ]);

        Menu::query()->updateOrCreate(['link' => 'clientes.create'], [
            'parent_id' => $parent->getKey(),
            'ability' => 'Crear clientes',
            'title' => 'Crear nuevos clientes',
        ]);

        Menu::query()->updateOrCreate(['link' => 'clientes.trash'], [
            'parent_id' => $parent->getKey(),
            'ability' => 'Restaurar clientes',
            'title' => 'Listado de clientes eliminados',
        ]);
    }

    private function venta()
    {
        $parent = Menu::query()->updateOrCreate(['title' => 'Venta'], [
            'icon' => 'cil-credit-card',
        ]);

        Menu::query()->updateOrCreate(['link' => 'ventas.index'], [
            'parent_id' => $parent->getKey(),
            'ability' => 'Listar ventas',
            'title' => 'Listado de ventas',
        ]);

        Menu::query()->updateOrCreate(['link' => 'ventas.create'], [
            'parent_id' => $parent->getKey(),
            'ability' => 'Crear ventas',
            'title' => 'Crear nuevas ventas',
        ]);
    }
}
