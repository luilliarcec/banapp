<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class SeguridadSeeder extends Seeder
{
    public function run()
    {
        $this->empresa();
        $this->parametos();
        $this->usuarios();
        $this->roles();
        $this->lotes();
        $this->variedades();
        $this->tipos();
        $this->insumos();
        $this->produccion();
        $this->clientes();
        $this->venta();
        $this->admin();
    }

    private function empresa()
    {
        Permission::updateOrCreate(['name' => 'Ver empresa']);
        Permission::updateOrCreate(['name' => 'Actualizar empresa']);
    }

    private function parametos()
    {
        Permission::updateOrCreate(['name' => 'Ver parámetros']);
        Permission::updateOrCreate(['name' => 'Actualizar parámetros']);
    }

    private function usuarios()
    {
        Permission::updateOrCreate(['name' => 'Listar usuarios']);
        Permission::updateOrCreate(['name' => 'Crear usuarios']);
        Permission::updateOrCreate(['name' => 'Editar usuarios']);
        Permission::updateOrCreate(['name' => 'Eliminar usuarios']);
        Permission::updateOrCreate(['name' => 'Restaurar usuarios']);
    }

    private function roles()
    {
        Permission::updateOrCreate(['name' => 'Listar roles']);
        Permission::updateOrCreate(['name' => 'Crear roles']);
        Permission::updateOrCreate(['name' => 'Editar roles']);
        Permission::updateOrCreate(['name' => 'Eliminar roles']);
    }

    private function lotes()
    {
        Permission::updateOrCreate(['name' => 'Listar lotes']);
        Permission::updateOrCreate(['name' => 'Crear lotes']);
        Permission::updateOrCreate(['name' => 'Editar lotes']);
        Permission::updateOrCreate(['name' => 'Eliminar lotes']);
        Permission::updateOrCreate(['name' => 'Restaurar lotes']);
    }

    private function variedades()
    {
        Permission::updateOrCreate(['name' => 'Listar variedades']);
        Permission::updateOrCreate(['name' => 'Crear variedades']);
        Permission::updateOrCreate(['name' => 'Editar variedades']);
        Permission::updateOrCreate(['name' => 'Eliminar variedades']);
        Permission::updateOrCreate(['name' => 'Restaurar variedades']);
    }

    private function tipos()
    {
        Permission::updateOrCreate(['name' => 'Listar tipos']);
        Permission::updateOrCreate(['name' => 'Crear tipos']);
        Permission::updateOrCreate(['name' => 'Editar tipos']);
    }

    private function insumos()
    {
        Permission::updateOrCreate(['name' => 'Listar insumos']);
        Permission::updateOrCreate(['name' => 'Crear insumos']);
        Permission::updateOrCreate(['name' => 'Editar insumos']);
        Permission::updateOrCreate(['name' => 'Eliminar insumos']);
        Permission::updateOrCreate(['name' => 'Restaurar insumos']);
    }

    private function produccion()
    {
        Permission::updateOrCreate(['name' => 'Listar producciones']);
        Permission::updateOrCreate(['name' => 'Crear producciones']);
    }

    private function clientes()
    {
        Permission::updateOrCreate(['name' => 'Listar clientes']);
        Permission::updateOrCreate(['name' => 'Crear clientes']);
        Permission::updateOrCreate(['name' => 'Editar clientes']);
        Permission::updateOrCreate(['name' => 'Eliminar clientes']);
        Permission::updateOrCreate(['name' => 'Restaurar clientes']);
    }

    private function venta()
    {
        Permission::updateOrCreate(['name' => 'Listar ventas']);
        Permission::updateOrCreate(['name' => 'Crear ventas']);
    }

    private function admin()
    {
        Role::updateOrCreate(['name' => 'Administrador'])
            ->givePermissionTo(Permission::all());

        User::updateOrCreate(
            [
                'email' => 'bm9140519@gmail.com'
            ],
            [
                'nombre' => 'Bryan Morales',
                'cedula' => '9999999999',
                'password' => 'master',
            ]
        )->assignRole('Administrador');
    }
}
