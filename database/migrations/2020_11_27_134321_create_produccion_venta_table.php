<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProduccionVentaTable extends Migration
{
    public function up()
    {
        Schema::create('produccion_venta', function (Blueprint $table) {
            $table->id('id');
            $table->foreignId('venta_id')->constrained('ventas');
            $table->foreignId('produccion_id')->constrained('producciones');
            $table->integer('cantidad');
            $table->decimal('precio');
        });
    }

    public function down()
    {
        Schema::dropIfExists('produccion_venta');
    }
}
