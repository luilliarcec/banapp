<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProduccionesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('producciones', function (Blueprint $table) {
            $table->id();
            $table->foreignId('lote_id')->constrained('lotes');
            $table->foreignId('variedad_id')->constrained('variedades');
            $table->decimal('precio_base_caja')->comment('Copia de parametros');
            $table->unsignedInteger('total_manos_caja')->comment('Copia de parametros');
            $table->decimal('porcentaje_utilidad')->comment('Copia de parametros');
            $table->date('fecha_produccion');
            $table->unsignedInteger('total_manos')->comment('Total de manos (buenas) producidas');
            $table->unsignedInteger('rechazos')->comment('Total de manos (rechazadas) producidas');
            $table->unsignedInteger('caja_totales')->comment('Total de cajas producidas');
            $table->unsignedInteger('existencia')->comment('Stock (Se reduce en ventas hasta llegar a 0)');
            $table->decimal('precio_caja_venta')->comment('Precio de caja total_produccion/caja_totales > precio_base_caja');
            $table->decimal('costo_produccion')->comment('Sumatoria de los costos de insumos + valores no registrados');
            $table->decimal('total_produccion')->comment('Valor de costo_produccion + porcentaje_utilidad');
            $table->text('observacion')->comment('Información adicional de la producción');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('producciones');
    }
}
