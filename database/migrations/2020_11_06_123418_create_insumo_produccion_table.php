<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInsumoProduccionTable extends Migration
{
    public function up()
    {
        Schema::create('insumo_produccion', function (Blueprint $table) {
            $table->id('id');
            $table->foreignId('produccion_id')->constrained('producciones');
            $table->foreignId('insumo_id')->constrained('insumos');
            $table->integer('cantidad');
            $table->decimal('costo_referencial');
        });
    }

    public function down()
    {
        Schema::dropIfExists('insumo_produccion');
    }
}
