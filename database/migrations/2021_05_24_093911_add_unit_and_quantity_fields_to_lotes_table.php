<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddUnitAndQuantityFieldsToLotesTable extends Migration
{
    public function up()
    {
        Schema::table('lotes', function (Blueprint $table) {
            $table->float('cantidad')->default(0);
            $table->string('unidad')->default('Hectáreas');
        });
    }

    public function down()
    {
        Schema::table('lotes', function (Blueprint $table) {
            $table->dropColumn('cantidad');
            $table->dropColumn('unidad');
        });
    }
}
