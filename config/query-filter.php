<?php

return [
    'icons' => [

        'up' => 'cil-arrow-top',

        'down' => 'cil-arrow-bottom',

        'sort' => 'cil-swap-vertical'

    ]
];
