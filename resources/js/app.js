/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');
import vSelect from 'vue-select';
import money from 'v-money';
import BDatepicker from 'vue-bootstrap-datetimepicker';

window.toastr = require('toastr');

toastr.options = {
    "closeButton": true,
    "progressBar": true,
    "positionClass": "toast-top-right",
};

window.Vue = require('vue');
Vue.component('v-select', vSelect);
Vue.use(BDatepicker);
Vue.use(money, {precision: 2, decimal: ',', thousands: '.'});

Vue.prototype.$currency_format = function (number) {
    return '$ '.concat(
        new Intl.NumberFormat("de-DE", {maximumFractionDigits: 2, minimumFractionDigits: 2}).format(number)
    );
};

Vue.prototype.$configDatepicker = {
    locale: 'es',
    format: 'YYYY-MM-DD',
    ignoreReadonly: true,
    maxDate: new Date(),
};

Vue.prototype.$now = function () {
    let date = new Date();
    return date.getFullYear() + '-' + (date.getMonth() + 1) + '-' + date.getDate();
};

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

Vue.component('create-produccion', require('./Pages/CreateProduccion.vue').default);
Vue.component('create-venta', require('./Pages/CreateVenta.vue').default);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app',
});
