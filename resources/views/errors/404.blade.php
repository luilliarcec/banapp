@extends('layouts.error')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-6">
                <div class="clearfix">
                    <h1 class="float-left display-3 mr-4">404</h1>
                    <h4 class="pt-3">Ops! Parece que te has perdido.</h4>
                    <p class="text-muted">La página que estás buscando parece que no existe.</p>

                    <a href="{{ route('app.home') }}" class="btn btn-info" type="button">Ir al inicio</a>
                </div>
            </div>
        </div>
    </div>
@endsection
