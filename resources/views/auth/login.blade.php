@extends('layouts.auth')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-6">
                <div class="card-group">
                    <div class="card p-4">
                        <div class="card-body">
                            <h1>Iniciar sesión</h1>
                            <p class="text-muted">Ingresa con tu cuenta</p>
                            <form method="POST" action="{{ route('login') }}">
                                @csrf

                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text">
                                            <svg class="c-icon">
                                                <use
                                                    xlink:href="assets/icons/coreui/free-symbol-defs.svg#cui-user"></use>
                                            </svg>
                                        </div>
                                    </div>

                                    <input class="form-control {{ $errors->any() ? 'is-invalid' : '' }}" type="text"
                                           placeholder="{{ __('E-Mail Address') }}"
                                           name="email" value="{{ old('email') }}" aria-label="email" required
                                           autofocus>

                                    @error('email')
                                    <div class="invalid-feedback d-block">{{ $message }}</div>
                                    @enderror
                                </div>

                                <div class="input-group mb-4">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text">
                                            <svg class="c-icon">
                                                <use
                                                    xlink:href="assets/icons/coreui/free-symbol-defs.svg#cui-lock-locked"></use>
                                            </svg>
                                        </div>
                                    </div>

                                    <input class="form-control" type="password" placeholder="{{ __('Password') }}"
                                           name="password" aria-label="password" required>
                                </div>


                                <div class="row">
                                    <div class="col-6">
                                        <button class="btn btn-primary px-4" type="submit">{{ __('Login') }}</button>
                                    </div>

                                    <div class="col-12">
                                        <a href="{{ route('password.request') }}"
                                           class="btn btn-link px-0">{{ __('Forgot Your Password?') }}</a>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
