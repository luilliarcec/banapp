@foreach($links as $item)
    @if ($item->childrens->count() > 0)
        @canany($item->childrens->pluck('ability')->toArray())
            <li class="c-sidebar-nav-dropdown">
                <a class="c-sidebar-nav-dropdown-toggle" href="#">
                    @if($item->icon)
                        <i class='{{ $item->icon }} c-sidebar-nav-icon'></i>
                    @endif
                    {{ $item->title }}
                </a>
                <ul class="c-sidebar-nav-dropdown-items">
                    @include('layouts.partials._links', ['links' => $item->childrens])
                </ul>
            </li>
        @endcan
    @else
        @can($item->ability)
            <li class="c-sidebar-nav-item">
                <a class='c-sidebar-nav-link' href='{{ route($item->link) }}'>
                    <span class="c-sidebar-nav-icon"></span>
                    {{ $item->title }}
                </a>
            </li>
        @endcan
    @endif
@endforeach
