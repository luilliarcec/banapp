<div class="c-sidebar-brand">
    <img class="c-sidebar-brand-full" src="{{ asset('assets/img/bananapp.letras.svg') }}"
         width="100%" height="46" alt="CoreUI Logo">
</div>

<ul class="c-sidebar-nav">
    @include('layouts.partials._links', ['links' => $sidebar])
</ul>

<button class="c-sidebar-minimizer c-class-toggler" type="button" data-target="_parent"
        data-class="c-sidebar-minimized">
</button>
