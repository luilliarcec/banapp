<header class="c-header c-header-light c-header-fixed c-header-with-subheader">
    <button class="c-header-toggler c-class-toggler d-lg-none mr-auto" type="button" data-target="#sidebar"
            data-class="c-sidebar-show">
        <span class="c-header-toggler-icon"></span>
    </button>

    <button class="c-header-toggler c-class-toggler ml-3 d-md-down-none" type="button" data-target="#sidebar"
            data-class="c-sidebar-lg-show" responsive="true">
        <span class="c-header-toggler-icon"></span>
    </button>

    <ul class="c-header-nav ml-auto mr-4">
        <li class="c-header-nav-item dropdown">
            <a class="c-header-nav-link" data-toggle="dropdown" href="#"
               role="button" aria-haspopup="true" aria-expanded="false">

                <div class="c-avatar">
                    <img class="c-avatar-img" src="{{ url('/assets/img/user.png') }}"
                         alt="{{ auth()->user()->email }}">
                </div>
            </a>

            <div class="dropdown-menu dropdown-menu-right pt-0">
                <a class="dropdown-item" href="javascript:void(0)">
                    <b>{{ auth()->user()->nombre }}</b>
                </a>

                <div class="dropdown-header bg-light py-2">
                    <strong>Configuración</strong>
                </div>

                <a class="dropdown-item" href="#">
                    <svg class="c-icon mr-2">
                        <use xlink:href="{{ url('/icons/sprites/free.svg#cil-user') }}"></use>
                    </svg>
                    Perfil
                </a>

                <a class="dropdown-item" href="#">
                    <svg class="c-icon mr-2">
                        <use xlink:href="{{ url('/icons/sprites/free.svg#cil-account-logout') }}"></use>
                    </svg>

                    <form action="{{ url('/logout') }}" method="POST"> @csrf
                        <button type="submit" class="btn btn-ghost-dark btn-block">Logout</button>
                    </form>
                </a>
            </div>
        </li>
    </ul>
</header>
