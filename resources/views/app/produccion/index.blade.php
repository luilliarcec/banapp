<x-app>
    <x-breadcrumps.wrapper title="Listado de producciones" create="{{ route('produccion.create') }}"
                           ability="produccion" text="Nueva producción">
        <x-slot name="afterButtons">
            <x-buttons.reports
                xlsx="{!! route('produccion.informe', request()->query()) !!}">
            </x-buttons.reports>
        </x-slot>
    </x-breadcrumps.wrapper>

    <x-card>
        <x-slot name="title">Producción</x-slot>
        <x-slot name="description">Lista las producciones.</x-slot>

        <x-searchers.default text="true" dateRange="true"></x-searchers.default>

        <x-table :collection="$data">
            <x-slot name="header">
                <th>Lote</th>
                <th>Variedad</th>
                <x-th name="precio_base_caja">Precio base de caja</x-th>
                <x-th name="total_manos_caja">Total de manos por caja</x-th>
                <x-th name="fecha_produccion">Fecha de producción</x-th>
                <x-th name="total_manos">Total de manos producidas</x-th>
                <x-th name="rechazos">Total de manos rechazadas</x-th>
                <x-th name="caja_totales">Total de cajas producidas</x-th>
                <x-th name="existencia">Existencia de cajas</x-th>
                <x-th name="precio_caja_venta">Precio de venta</x-th>
                <x-th name="costo_produccion">Costo de producción</x-th>
                <x-th name="total_produccion">Total de producción con utilidad</x-th>
                <th>Acciones</th>
            </x-slot>

            <x-slot name="body">
                @forelse($data as $item)
                    <tr>
                        <td>{{ $item->lote->nombre }}</td>
                        <td>{{ $item->variedad->nombre }}</td>
                        <td>{{ $item->precio_base_caja }}</td>
                        <td>{{ $item->total_manos_caja }}</td>
                        <td>{{ $item->fecha_produccion->isoFormat('ll') }}</td>
                        <td>{{ $item->total_manos }}</td>
                        <td>{{ $item->rechazos }}</td>
                        <td>{{ $item->caja_totales }}</td>
                        <td>{{ $item->existencia }}</td>
                        <td>{{ $item->moneyFormat('precio_caja_venta') }}</td>
                        <td>{{ $item->moneyFormat('costo_produccion') }}</td>
                        <td>{{ $item->moneyFormat('total_produccion') }}</td>
                        <td>
                            <x-buttons.resources :model="$item" show="produccion.show">
                            </x-buttons.resources>
                        </td>
                    </tr>
                @empty
                    <tr>
                        <th class="text-center" colspan="15">Sin registros</th>
                    </tr>
                @endforelse
            </x-slot>
        </x-table>
    </x-card>
</x-app>
