<x-app>
    <x-breadcrumps.wrapper title="Registrar producción"
                           back="{{ route('produccion.index') }}"></x-breadcrumps.wrapper>

    <x-card class="col-12">
        <x-slot name="title">Producción</x-slot>
        <x-slot name="description">Ingrese los datos de la producción</x-slot>

        <div class="mt-3">
            <create-produccion :parametros="{{ $parametros }}"
                               :lotes="{{ $lotes }}"
                               :variedades="{{ $variedades }}"
                               :insumos="{{ $insumos }}"></create-produccion>
        </div>
    </x-card>
</x-app>
