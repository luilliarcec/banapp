<x-app>
    <x-breadcrumps.wrapper title="Información de la producción"></x-breadcrumps.wrapper>

    <x-card class="col-md-12">
        <x-slot name="title">Producción</x-slot>
        <x-slot name="description">Información de la producción</x-slot>

        <form class="form-horizontal mt-4" role="form">
            <div class="form-body">
                <h5 class="box-title">Información general</h5>
                <hr class="mt-0 mb-5">

                <div class="row">
                    <x-presenters.show-field label="Lote">
                        {{ $produccion->lote->nombre }}
                    </x-presenters.show-field>

                    <x-presenters.show-field label="Variedad">
                        {{ $produccion->variedad->nombre }}
                    </x-presenters.show-field>

                    <x-presenters.show-field label="Precio base de caja">
                        {{ $produccion->precio_base_caja }}
                    </x-presenters.show-field>

                    <x-presenters.show-field label="Total de manos por caja">
                        {{ $produccion->total_manos_caja }}
                    </x-presenters.show-field>

                    <x-presenters.show-field label="Fecha de producción">
                        {{ $produccion->fecha_produccion->isoFormat('ll') }}
                    </x-presenters.show-field>

                    <x-presenters.show-field label="Total de manos producidas">
                        {{ $produccion->total_manos }}
                    </x-presenters.show-field>

                    <x-presenters.show-field label="Total de manos rechazadas">
                        {{ $produccion->rechazos }}
                    </x-presenters.show-field>

                    <x-presenters.show-field label="Total de cajas producidas">
                        {{ $produccion->caja_totales }}
                    </x-presenters.show-field>

                    <x-presenters.show-field label="Existencia de cajas">
                        {{ $produccion->existencia }}
                    </x-presenters.show-field>

                    <x-presenters.show-field label="Precio de venta">
                        {{ $produccion->precio_caja_venta }}
                    </x-presenters.show-field>

                    <x-presenters.show-field label="Costo de producción">
                        {{ $produccion->costo_produccion }}
                    </x-presenters.show-field>

                    <x-presenters.show-field label="Total de producción con utilidad">
                        {{ $produccion->total_produccion }}
                    </x-presenters.show-field>

                    <x-presenters.show-field label="Observación">
                        {{ $produccion->observacion }}
                    </x-presenters.show-field>
                </div>

                <div class="row">
                    <div class="col-12">
                        <x-table :collection="$produccion->insumos">
                            <x-slot name="header">
                                <th>Insumo</th>
                                <th>Cantidad</th>
                                <th>Costo referencial</th>
                            </x-slot>

                            <x-slot name="body">
                                @forelse($produccion->insumos as $insumo)
                                    <tr>
                                        <td>{{ $insumo->nombre }}</td>
                                        <td>{{ $insumo->pivot->cantidad }}</td>
                                        <td>{{ $insumo->pivot->costo_referencial }}</td>
                                    </tr>
                                @empty
                                    <tr>
                                        <th class="text-center" colspan="15">Sin registros</th>
                                    </tr>
                                @endforelse
                            </x-slot>
                        </x-table>
                    </div>
                </div>
            </div>
        </form>
    </x-card>
</x-app>
