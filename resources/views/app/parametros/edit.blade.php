<x-app>
    <x-breadcrumps.wrapper title="Actualizar los parámetros generales"
                           back="{{ route('parametros.show') }}"></x-breadcrumps.wrapper>

    <x-card class="col-md-6">
        <x-slot name="title">Parámetros</x-slot>
        <x-slot name="description">Actualice los parámetros generales</x-slot>

        <x-form class="mt-5" method="put" :model="$parametro" action="{{ route('parametros.update', $parametro) }}">
            <div class="row">
                <div class="col-12">
                    <x-input type="number" name="precio_base_caja" label="Precio base de caja" step="0.01"
                             required></x-input>
                </div>

                <div class="col-12">
                    <x-input type="number" name="total_manos_caja" label="Total de manos por caja"
                             required></x-input>
                </div>

                <div class="col-12">
                    <x-input type="number" name="porcentaje_utilidad" label="Porcentaje de utilidad" step="0.01"
                             required></x-input>
                </div>

                <div class="col-12">
                    <x-input type="number" name="porcentaje_iva" label="Porcentaje de IVA" step="0.01"
                             required></x-input>
                </div>

                <div class="col-12 mt-3">
                    <button type="submit" class="btn btn-success w-100">Actualizar</button>
                </div>
            </div>
        </x-form>
    </x-card>

    <x-slot name="js">
        @if(session('message'))
            <script>
                document.addEventListener("DOMContentLoaded", function (event) {
                    Swal.fire('¡Gran trabajo!', "{{ session('message') }}", 'success');
                });
            </script>
        @endif
    </x-slot>
</x-app>
