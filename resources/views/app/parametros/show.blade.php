<x-app>
    <x-breadcrumps.wrapper title="Parámetros generales"></x-breadcrumps.wrapper>

    <x-card class="col-md-12">
        <x-slot name="title">Parámetros</x-slot>
        <x-slot name="description">Datos de los parámetros generales</x-slot>

        <form class="form-horizontal mt-4" role="form">
            <div class="form-body">
                <h5 class="box-title">Información general</h5>
                <hr class="mt-0 mb-5">

                <div class="row">
                    <x-presenters.show-field label="Precio base de caja">
                        $ {{ $parametro->precio_base_caja }}
                    </x-presenters.show-field>

                    <x-presenters.show-field label="Total de manos por caja">
                        {{ $parametro->total_manos_caja }}
                    </x-presenters.show-field>

                    <x-presenters.show-field label="Porcentaje de utilidad">
                        {{ $parametro->porcentaje_utilidad }} %
                    </x-presenters.show-field>

                    <x-presenters.show-field label="Porcentaje de IVA">
                        {{ $parametro->porcentaje_iva }} %
                    </x-presenters.show-field>
                </div>
            </div>

            <div class="form-actions">
                <div class="row">
                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-md-offset-3 col-md-9">
                                <a href="{{ route('parametros.edit') }}" class="btn btn-info">
                                    <i class="cil-pencil"></i> Editar
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </x-card>
</x-app>
