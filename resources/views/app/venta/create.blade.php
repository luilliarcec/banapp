<x-app>
    <x-breadcrumps.wrapper title="Registrar venta"
                           back="{{ route('ventas.index') }}"></x-breadcrumps.wrapper>

    <x-card class="col-12">
        <x-slot name="title">Venta</x-slot>
        <x-slot name="description">Ingrese los datos de la venta</x-slot>

        <div class="mt-3">
            <create-venta :parametros="{{ $parametros }}"
                          :clientes="{{ $clientes }}"
                          :variedades="{{ $variedades }}"></create-venta>
        </div>
    </x-card>
</x-app>
