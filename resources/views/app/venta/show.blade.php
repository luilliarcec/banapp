<x-app>
    <x-breadcrumps.wrapper title="Información de la venta"></x-breadcrumps.wrapper>

    <x-card class="col-md-12">
        <x-slot name="title">Venta</x-slot>
        <x-slot name="description">Información de la venta</x-slot>

        <form class="form-horizontal mt-4" role="form">
            <div class="form-body">
                <h5 class="box-title">Información general</h5>
                <hr class="mt-0 mb-5">

                <div class="row">
                    <x-presenters.show-field label="Venta #">
                        {{ $venta->getKey() }}
                    </x-presenters.show-field>

                    <x-presenters.show-field label="Cliente">
                        {{ $venta->cliente->nombre }}
                    </x-presenters.show-field>

                    <x-presenters.show-field label="Fecha de venta">
                        {{ $venta->fecha_venta->isoFormat('ll') }}
                    </x-presenters.show-field>

                    <x-presenters.show-field label="Vendedor">
                        {{ $venta->vendedor->nombre }}
                    </x-presenters.show-field>
                </div>

                <div class="row">
                    <div class="col-12">
                        <x-table :collection="$venta->cajas">
                            <x-slot name="header">
                                <th>Banano</th>
                                <th>Cantidad</th>
                                <th>Precio</th>
                            </x-slot>

                            <x-slot name="body">
                                @forelse($venta->cajas as $caja)
                                    <tr>
                                        <td>{{ $caja->variedad->descripcion }}</td>
                                        <td>{{ $caja->pivot->cantidad }}</td>
                                        <td class="text-right">{{ $caja->pivot->precio }}</td>
                                    </tr>
                                @empty
                                    <tr>
                                        <th class="text-center" colspan="15">Sin registros</th>
                                    </tr>
                                @endforelse

                                <tr>
                                    <th class="text-right" colspan="2">Subtotal:</th>
                                    <th class="text-right">{{ $venta->moneyFormat('subtotal') }}</th>
                                </tr>

                                <tr>
                                    <th class="text-right" colspan="2">IVA {{ $venta->porcentaje_iva }}%:</th>
                                    <th class="text-right">{{ $venta->moneyFormat('iva') }}</th>
                                </tr>

                                <tr>
                                    <th class="text-right" colspan="2">Total:</th>
                                    <th class="text-right">{{ $venta->moneyFormat('total') }}</th>
                                </tr>
                            </x-slot>
                        </x-table>
                    </div>
                </div>

                <div class="row">
                    <div class="col-6">
                        <a href="{{ route('ventas.factura', $venta) }}" class="btn btn-secondary" target="_blank">
                            <i class="cil-print"></i> Imprimir factura
                        </a>
                    </div>
                </div>
            </div>
        </form>
    </x-card>
</x-app>
