<x-app>
    <x-breadcrumps.wrapper title="Listado de ventas" create="{{ route('ventas.create') }}"
                           ability="venta" text="Nueva venta">
        <x-slot name="afterButtons">
            <x-buttons.reports
                xlsx="{!! route('ventas.informe', request()->query()) !!}">
            </x-buttons.reports>
        </x-slot>
    </x-breadcrumps.wrapper>

    <x-card>
        <x-slot name="title">Venta</x-slot>
        <x-slot name="description">Lista las ventas.</x-slot>

        <x-searchers.default text="true" dateRange="true"></x-searchers.default>

        <x-table :collection="$data">
            <x-slot name="header">
                <th>Cliente</th>
                <th>Vendedor</th>
                <x-th name="fecha_venta">Fecha de venta</x-th>
                <th>Subtotal</th>
                <th>Porcentaje de IVA</th>
                <th>IVA</th>
                <th>Total</th>
                <th>Acciones</th>
            </x-slot>

            <x-slot name="body">
                @forelse($data as $item)
                    <tr>
                        <td>{{ $item->cliente->nombre }}</td>
                        <td>{{ $item->vendedor->nombre }}</td>
                        <td>{{ $item->fecha_venta->isoFormat('ll') }}</td>
                        <td>{{ $item->moneyFormat('subtotal') }}</td>
                        <td>{{ $item->moneyFormat('porcentaje_iva') }} %</td>
                        <td>{{ $item->moneyFormat('iva') }}</td>
                        <td>{{ $item->moneyFormat('total') }}</td>
                        <td>
                            <x-buttons.resources :model="$item" show="ventas.show">
                            </x-buttons.resources>
                        </td>
                    </tr>
                @empty
                    <tr>
                        <th class="text-center" colspan="15">Sin registros</th>
                    </tr>
                @endforelse
            </x-slot>
        </x-table>
    </x-card>
</x-app>
