<x-app>
    <x-breadcrumps.wrapper title="Crear nuevo cliente" back="{{ route('clientes.index') }}">
    </x-breadcrumps.wrapper>

    <x-card class="col-md-6">
        <x-slot name="title">Clientes</x-slot>
        <x-slot name="description">Ingrese los datos del cliente</x-slot>

        <x-form class="mt-5" method="post" action="{{ route('clientes.store') }}">
            <div class="row">
                <div class="col-12">
                    <x-input type="text" name="nombre" label="Nombres" maxlength="255" required></x-input>
                </div>

                <div class="col-12">
                    <x-input type="text" name="cedula" label="Cédula" maxlength="10" onkeypress="return digits(event)"
                             required></x-input>
                </div>

                <div class="col-12">
                    <x-input type="text" name="telefono" label="Teléfono" maxlength="20"
                             onkeypress="return digits(event)"></x-input>
                </div>

                <div class="col-12">
                    <x-input type="text" name="direccion" label="Dirección" maxlength="255"></x-input>
                </div>

                <div class="col-12">
                    <x-input type="email" name="email" label="Correo electrónico" maxlength="255" required></x-input>
                </div>

                <div class="col-12 mt-3">
                    <button type="submit" class="btn btn-success w-100">Guardar</button>
                </div>
            </div>
        </x-form>
    </x-card>

    <x-slot name="js">
        @if(session('message'))
            <script>
                document.addEventListener("DOMContentLoaded", function (event) {
                    Swal.fire('¡Gran trabajo!', "{{ session('message') }}", 'success');
                });
            </script>
        @endif
    </x-slot>
</x-app>
