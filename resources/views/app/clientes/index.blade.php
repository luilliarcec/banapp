<x-app>
    @if (request()->routeIs('clientes.trash'))
        <x-breadcrumps.wrapper title="Listado de clientes eliminados"></x-breadcrumps.wrapper>
    @else
        <x-breadcrumps.wrapper title="Listado de clientes" create="{{ route('clientes.create') }}"
                               ability="clientes" text="Nuevo cliente">
            <x-slot name="afterButtons">
                <x-buttons.reports
                    xlsx="{!! route('clientes.informe', request()->query()) !!}">
                </x-buttons.reports>
            </x-slot>
        </x-breadcrumps.wrapper>
    @endif

    <x-card>
        <x-slot name="title">Clientes</x-slot>
        <x-slot name="description">Lista los clientes.</x-slot>

        <x-searchers.default text="true"></x-searchers.default>

        <x-table :collection="$data">
            <x-slot name="header">
                <x-th name="nombre">Nombre</x-th>
                <x-th name="cedula">Cédula</x-th>
                <x-th name="email">Email</x-th>
                <th>Teléfono</th>
                <th>Dirección</th>
                <x-th name="created_at">Fecha de creación</x-th>
                <th>Acciones</th>
            </x-slot>

            <x-slot name="body">
                @forelse($data as $item)
                    <tr>
                        <td>{{ $item->nombre }}</td>
                        <td>{{ $item->cedula }}</td>
                        <td>{{ $item->email }}</td>
                        <td>{{ $item->telefono }}</td>
                        <td>{{ $item->direccion }}</td>
                        <td>{{ $item->created_at->isoFormat('ll') }}</td>
                        <td>
                            <x-buttons.resources :model="$item" ability="clientes"
                                                 edit="clientes.edit"
                                                 destroy="clientes.destroy"
                                                 restore="clientes.restore">
                            </x-buttons.resources>
                        </td>
                    </tr>
                @empty
                    <tr>
                        <th class="text-center" colspan="15">Sin registros</th>
                    </tr>
                @endforelse
            </x-slot>
        </x-table>
    </x-card>
</x-app>
