<x-app>
    @if (request()->routeIs('roles.trash'))
        <x-breadcrumps.wrapper title="Listado de roles eliminados"></x-breadcrumps.wrapper>
    @else
        <x-breadcrumps.wrapper title="Listado de roles" create="{{ route('roles.create') }}"
                               ability="roles" text="Nuevo rol">
        </x-breadcrumps.wrapper>
    @endif

    <x-card>
        <x-slot name="title">Roles</x-slot>
        <x-slot name="description">Lista los roles.</x-slot>

        <x-searchers.default text="true"></x-searchers.default>

        <x-table :collection="$data">
            <x-slot name="header">
                <x-th name="nombre">Rol</x-th>
                <th>Cantidad de permisos asignados</th>
                <x-th name="created_at">Fecha de creación</x-th>
                <th>Acciones</th>
            </x-slot>

            <x-slot name="body">
                @forelse($data as $item)
                    <tr>
                        <td>{{ $item->name }}</td>
                        <td>{{ $item->permissions->count() }}</td>
                        <td>{{ $item->created_at->isoFormat('ll') }}</td>
                        <td>
                            <x-buttons.resources :model="$item" ability="roles"
                                                 edit="roles.edit"
                                                 destroy="roles.destroy">
                            </x-buttons.resources>
                        </td>
                    </tr>
                @empty
                    <tr>
                        <th class="text-center" colspan="15">Sin registros</th>
                    </tr>
                @endforelse
            </x-slot>
        </x-table>
    </x-card>
</x-app>
