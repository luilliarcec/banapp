<x-app>
    <x-breadcrumps.wrapper title="Crear nuevo rol" back="{{ route('roles.index') }}">
    </x-breadcrumps.wrapper>

    <x-card class="col-12 col-md-6">
        <x-slot name="title">Roles</x-slot>
        <x-slot name="description">Ingrese los datos del rol</x-slot>

        <x-form class="mt-5" action="{{ route('roles.store') }}">
            <div class="row">
                <div class="col-12">
                    <x-input type="text" name="name" label="Rol" maxlength="70" required></x-input>
                </div>

                <div class="col-12">
                    <x-select name="permissions[]" label="Permisos" size="7" :options="$permissions" multiple
                              required></x-select>
                </div>

                <div class="col-12 mt-3">
                    <button type="submit" class="btn btn-success w-100">Guardar</button>
                </div>
            </div>
        </x-form>
    </x-card>

    <x-slot name="js">
        @if(session('message'))
            <script>
                document.addEventListener("DOMContentLoaded", function (event) {
                    Swal.fire('¡Gran trabajo!', "{{ session('message') }}", 'success');
                });
            </script>
        @endif
    </x-slot>
</x-app>
