<x-app>
    @if(request()->routeIs('lotes.trash'))
        <x-breadcrumps.wrapper title="Listado de lotes eliminados" back="{{ route('lotes.index') }}">
        </x-breadcrumps.wrapper>
    @else
        <x-breadcrumps.wrapper title="Listado de lotes" create="{{ route('lotes.create') }}"
                               ability="lotes" text="Nuevo lote">
        </x-breadcrumps.wrapper>
    @endif

    <x-card>
        <x-slot name="title">Lotes</x-slot>
        <x-slot name="description">Lista los lotes de terreno.</x-slot>

        <x-searchers.default text="true"></x-searchers.default>

        <x-table :collection="$data">
            <x-slot name="header">
                <x-th name="nombre">Nombre</x-th>
                <x-th name="cantidad">Cantidad</x-th>
                <x-th name="unidad">Unidad</x-th>
                <x-th name="descripcion">Descripción</x-th>
                <x-th name="created_at">Fecha de creación</x-th>
                <th>Acciones</th>
            </x-slot>

            <x-slot name="body">
                @forelse($data as $item)
                    <tr>
                        <td>{{ $item->nombre }}</td>
                        <td>{{ $item->cantidad }}</td>
                        <td>{{ $item->unidad }}</td>
                        <td>{{ $item->descripcion }}</td>
                        <td>{{ $item->created_at->isoFormat('ll') }}</td>
                        <td>
                            <x-buttons.resources :model="$item" ability="lotes"
                                                 edit="lotes.edit"
                                                 destroy="lotes.destroy"
                                                 restore="lotes.restore">
                            </x-buttons.resources>
                        </td>
                    </tr>
                @empty
                    <tr>
                        <th class="text-center" colspan="15">Sin registros</th>
                    </tr>
                @endforelse
            </x-slot>
        </x-table>
    </x-card>
</x-app>

