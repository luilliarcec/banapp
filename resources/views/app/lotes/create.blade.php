<x-app>
    <x-breadcrumps.wrapper title="Crear nuevo lote" back="{{ route('lotes.index') }}"></x-breadcrumps.wrapper>

    <x-card class="col-md-6">
        <x-slot name="title">Lotes</x-slot>
        <x-slot name="description">Ingrese los datos del lote</x-slot>

        <x-form class="mt-5" action="{{ route('lotes.store') }}">
            <div class="row">
                <div class="col-12">
                    <x-input type="text" name="nombre" label="Lote" maxlength="70" required></x-input>
                </div>

                <div class="col-12">
                    <x-input type="text" name="cantidad" label="Cantidad" required></x-input>
                </div>

                <div class="col-12">
                    <x-input type="text" name="unidad" label="Unidad" maxlength="70" required></x-input>
                </div>

                <div class="col-12">
                    <x-textarea name="descripcion" label="Descripción del lote" maxlength="255" rows="3"></x-textarea>
                </div>

                <div class="col-12 mt-3">
                    <button type="submit" class="btn btn-success w-100">Guardar</button>
                </div>
            </div>
        </x-form>
    </x-card>

    <x-slot name="js">
        @if(session('message'))
            <script>
                document.addEventListener("DOMContentLoaded", function (event) {
                    Swal.fire('¡Gran trabajo!', "{{ session('message') }}", 'success');
                });
            </script>
        @endif
    </x-slot>
</x-app>
