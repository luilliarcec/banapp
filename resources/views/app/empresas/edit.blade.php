<x-app>
    <x-breadcrumps.wrapper title="Actualizar información de la empresa"
                           back="{{ route('empresas.show') }}"></x-breadcrumps.wrapper>

    <x-card class="col-md-6">
        <x-slot name="title">Empresa</x-slot>
        <x-slot name="description">Actualice los datos de la empresa</x-slot>

        <x-form class="mt-5" method="put" :model="$empresa" action="{{ route('empresas.update', $empresa) }}">
            <div class="row">
                <div class="col-12">
                    <x-input type="text" name="razon_social" label="Razón social" maxlength="255" required></x-input>
                </div>

                <div class="col-12">
                    <x-input type="text" name="nombre_comercial" label="Nombre comercial" maxlength="255"
                             required></x-input>
                </div>

                <div class="col-12">
                    <x-input type="text" name="ruc" label="RUC" maxlength="13" required></x-input>
                </div>

                <div class="col-12">
                    <x-input type="text" name="telefono" label="Teléfono" minlength="9" maxlength="20"></x-input>
                </div>

                <div class="col-12">
                    <x-input type="text" name="direccion" label="Dirección" maxlength="255"></x-input>
                </div>

                <div class="col-12">
                    <x-input type="text" name="representante_legal" label="Representante legal" maxlength="255"
                             required></x-input>
                </div>

                <div class="col-12">
                    <x-input type="text" name="dni_representante_legal" label="Cédula del representante legal"
                             maxlength="10" required></x-input>
                </div>

                <div class="col-12 mt-3">
                    <button type="submit" class="btn btn-success w-100">Actualizar</button>
                </div>
            </div>
        </x-form>
    </x-card>

    <x-slot name="js">
        @if(session('message'))
            <script>
                document.addEventListener("DOMContentLoaded", function (event) {
                    Swal.fire('¡Gran trabajo!', "{{ session('message') }}", 'success');
                });
            </script>
        @endif
    </x-slot>
</x-app>
