<x-app>
    <x-breadcrumps.wrapper title="Información de la empresa"></x-breadcrumps.wrapper>

    <x-card class="col-md-12">
        <x-slot name="title">Empresa</x-slot>
        <x-slot name="description">Información de la empresa</x-slot>

        <form class="form-horizontal mt-4" role="form">
            <div class="form-body">
                <h5 class="box-title">Información general</h5>
                <hr class="mt-0 mb-5">

                <div class="row">
                    <x-presenters.show-field label="Razón Social">
                        {{ $empresa->razon_social }}
                    </x-presenters.show-field>

                    <x-presenters.show-field label="Nombre comercial">
                        {{ $empresa->nombre_comercial }}
                    </x-presenters.show-field>

                    <x-presenters.show-field label="RUC">
                        {{ $empresa->ruc }}
                    </x-presenters.show-field>

                    <x-presenters.show-field label="Teléfono">
                        {{ $empresa->telefono }}
                    </x-presenters.show-field>

                    <x-presenters.show-field label="Dirección">
                        {{ $empresa->direccion }}
                    </x-presenters.show-field>

                    <x-presenters.show-field label="Representante legal">
                        {{ $empresa->representante_legal }}
                    </x-presenters.show-field>

                    <x-presenters.show-field label="Cédula del representante legal">
                        {{ $empresa->dni_representante_legal }}
                    </x-presenters.show-field>
                </div>
            </div>

            <div class="form-actions">
                <div class="row">
                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-md-offset-3 col-md-9">
                                <a href="{{ route('empresas.edit') }}" class="btn btn-info">
                                    <i class="cil-pencil"></i> Editar
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </x-card>
</x-app>
