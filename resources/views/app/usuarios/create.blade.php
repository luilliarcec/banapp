<x-app>
    <x-breadcrumps.wrapper title="Crear nuevo usuario" back="{{ route('usuarios.index') }}">
    </x-breadcrumps.wrapper>

    <x-card class="col-md-12">
        <x-slot name="title">Usuarios</x-slot>
        <x-slot name="description">Ingrese los datos del usuario</x-slot>

        <x-form class="mt-5" action="{{ route('usuarios.store') }}">
            <div class="row">
                <div class="col-12 col-md-6">
                    <div class="row">
                        <div class="col-12">
                            <x-input type="text" name="nombre" label="Nombres" maxlength="255" required></x-input>
                        </div>

                        <div class="col-12">
                            <x-input type="text" name="cedula" label="Cédula" maxlength="10"
                                     onkeypress="return digits(event)" required></x-input>
                        </div>

                        <div class="col-12">
                            <x-input type="text" name="telefono" label="Teléfono" onkeypress="return digits(event)"
                                     maxlength="20"></x-input>
                        </div>

                        <div class="col-12">
                            <x-input type="text" name="direccion" label="Dirección" maxlength="255"></x-input>
                        </div>

                        <div class="col-12">
                            <x-input type="email" name="email" label="Correo electrónico" maxlength="255"
                                     required></x-input>
                        </div>

                        <div class="col-12">
                            <x-input type="password" name="password" label="Contraseña" maxlength="8"
                                     required></x-input>
                        </div>

                        <div class="col-12 mt-3">
                            <button type="submit" class="btn btn-success w-100">Guardar</button>
                        </div>
                    </div>
                </div>

                <div class="col-12 col-md-6">
                    <div class="row">
                        <div class="col-12">
                            <x-select name="permissions[]" label="Permisos" size="7" :options="$permissions"
                                      multiple></x-select>
                        </div>

                        <div class="col-12">
                            <x-select name="roles[]" label="Roles" size="7" :options="$roles" multiple></x-select>
                        </div>
                    </div>
                </div>
            </div>
        </x-form>
    </x-card>

    <x-slot name="js">
        @if(session('message'))
            <script>
                document.addEventListener("DOMContentLoaded", function (event) {
                    Swal.fire('¡Gran trabajo!', "{{ session('message') }}", 'success');
                });
            </script>
        @endif
    </x-slot>
</x-app>
