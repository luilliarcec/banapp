<x-app>
    <x-breadcrumps.wrapper title="Editar un tipo de insumo"
                           back="{{ route('tipos-insumo.index') }}"></x-breadcrumps.wrapper>

    <x-card class="col-md-6">
        <x-slot name="title">Tipos de insumos</x-slot>
        <x-slot name="description">Actualice los datos del tipo de insumo</x-slot>

        <x-form class="mt-5" method="put" :model="$tipo" action="{{ route('tipos-insumo.update', $tipo) }}">
            <div class="row">
                <div class="col-12">
                    <x-input type="text" name="nombre" label="Tipo de insumo" maxlength="70" required></x-input>
                </div>

                <div class="col-12">
                    <x-textarea name="descripcion" label="Descripción del tipo de insumo" maxlength="255"
                                rows="3"></x-textarea>
                </div>

                <div class="col-12 mt-3">
                    <button type="submit" class="btn btn-success w-100">Actualizar</button>
                </div>
            </div>
        </x-form>
    </x-card>

    <x-slot name="js">
        @if(session('message'))
            <script>
                document.addEventListener("DOMContentLoaded", function (event) {
                    Swal.fire('¡Gran trabajo!', "{{ session('message') }}", 'success');
                });
            </script>
        @endif
    </x-slot>
</x-app>
