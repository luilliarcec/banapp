<x-app>
    <x-breadcrumps.wrapper title="Crear nuevo tipo de insumo"
                           back="{{ route('tipos-insumo.index') }}"></x-breadcrumps.wrapper>

    <x-card class="col-md-6">
        <x-slot name="title">Tipos de insumos</x-slot>
        <x-slot name="description">Ingrese los datos del tipo de insumo</x-slot>

        <x-form class="mt-5" action="{{ route('tipos-insumo.store') }}">
            <div class="row">
                <div class="col-12">
                    <x-input type="text" name="nombre" label="Tipo de insumo" maxlength="70" required></x-input>
                </div>

                <div class="col-12">
                    <x-textarea name="descripcion" label="Descripción del tipo de insumo" maxlength="255"
                                rows="3"></x-textarea>
                </div>

                <div class="col-12 mt-3">
                    <button type="submit" class="btn btn-success w-100">Guardar</button>
                </div>
            </div>
        </x-form>
    </x-card>

    <x-slot name="js">
        @if(session('message'))
            <script>
                document.addEventListener("DOMContentLoaded", function (event) {
                    Swal.fire('¡Gran trabajo!', "{{ session('message') }}", 'success');
                });
            </script>
        @endif
    </x-slot>
</x-app>
