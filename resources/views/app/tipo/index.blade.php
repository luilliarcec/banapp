<x-app>
    <x-breadcrumps.wrapper title="Listado de tipos de insumos" create="{{ route('tipos-insumo.create') }}"
                           ability="tipos" text="Nuevo tipo de insumo">
    </x-breadcrumps.wrapper>

    <x-card>
        <x-slot name="title">Tipos de insumos</x-slot>
        <x-slot name="description">Lista los tipos de insumos.</x-slot>

        <x-searchers.default text="true"></x-searchers.default>

        <x-table :collection="$data">
            <x-slot name="header">
                <x-th name="nombre">Nombre</x-th>
                <x-th name="descripcion">Descripción</x-th>
                <x-th name="created_at">Fecha de creación</x-th>
                <th>Acciones</th>
            </x-slot>

            <x-slot name="body">
                @forelse($data as $item)
                    <tr>
                        <td>{{ $item->nombre }}</td>
                        <td>{{ $item->descripcion }}</td>
                        <td>{{ $item->created_at->isoFormat('ll') }}</td>
                        <td>
                            <x-buttons.resources :model="$item" ability="tipos" edit="tipos-insumo.edit">
                            </x-buttons.resources>
                        </td>
                    </tr>
                @empty
                    <tr>
                        <th class="text-center" colspan="15">Sin registros</th>
                    </tr>
                @endforelse
            </x-slot>
        </x-table>
    </x-card>
</x-app>

