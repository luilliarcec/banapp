<x-app>
    @if(request()->routeIs('variedades.trash'))
        <x-breadcrumps.wrapper title="Listado de variedades eliminados" back="{{ route('variedades.index') }}">
        </x-breadcrumps.wrapper>
    @else
        <x-breadcrumps.wrapper title="Listado de variedades" create="{{ route('variedades.create') }}"
                               ability="variedades" text="Nueva variedad">
        </x-breadcrumps.wrapper>
    @endif

    <x-card>
        <x-slot name="title">Variedades</x-slot>
        <x-slot name="description">Lista las variedades de banano.</x-slot>

        <x-searchers.default text="true"></x-searchers.default>

        <x-table :collection="$data">
            <x-slot name="header">
                <x-th name="nombre">Nombre</x-th>
                <x-th name="descripcion">Descripción</x-th>
                <x-th name="created_at">Fecha de creación</x-th>
                <th>Acciones</th>
            </x-slot>

            <x-slot name="body">
                @forelse($data as $item)
                    <tr>
                        <td>{{ $item->nombre }}</td>
                        <td>{{ $item->descripcion }}</td>
                        <td>{{ $item->created_at->isoFormat('ll') }}</td>
                        <td>
                            <x-buttons.resources :model="$item" ability="variedades"
                                                 edit="variedades.edit"
                                                 destroy="variedades.destroy"
                                                 restore="variedades.restore">
                            </x-buttons.resources>
                        </td>
                    </tr>
                @empty
                    <tr>
                        <th class="text-center" colspan="15">Sin registros</th>
                    </tr>
                @endforelse
            </x-slot>
        </x-table>
    </x-card>
</x-app>
