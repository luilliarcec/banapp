<x-app>
    <x-breadcrumps.wrapper title="Editar un insumo" back="{{ route('insumos.index') }}"></x-breadcrumps.wrapper>

    <x-card class="col-md-6">
        <x-slot name="title">Insumos</x-slot>
        <x-slot name="description">Actualice los datos del insumo</x-slot>

        <x-form class="mt-5" method="put" :model="$insumo" action="{{ route('insumos.update', $insumo) }}">
            <div class="row">
                <div class="col-12">
                    <x-input type="text" name="nombre" label="Insumo" maxlength="70" required></x-input>
                </div>

                <div class="col-12">
                    <x-select name="tipo_id" label="Tipo de insumo" empty="-- Seleccione un tipo --" :options="$tipos"
                              required></x-select>
                </div>

                <div class="col-12">
                    <x-input type="number" name="existencia" min="0" required></x-input>
                </div>

                <div class="col-12">
                    <x-input type="number" name="costo_referencial" min="0" step="0.01" required></x-input>
                </div>

                <div class="col-12">
                    <x-textarea name="descripcion" label="Descripción del insumo" maxlength="255" rows="3"></x-textarea>
                </div>

                <div class="col-12 mt-3">
                    <button type="submit" class="btn btn-success w-100">Actualizar</button>
                </div>
            </div>
        </x-form>
    </x-card>

    <x-slot name="js">
        @if(session('message'))
            <script>
                document.addEventListener("DOMContentLoaded", function (event) {
                    Swal.fire('¡Gran trabajo!', "{{ session('message') }}", 'success');
                });
            </script>
        @endif
    </x-slot>
</x-app>
