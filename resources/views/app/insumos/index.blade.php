<x-app>
    @if(request()->routeIs('insumos.trash'))
        <x-breadcrumps.wrapper title="Listado de insumos eliminados" back="{{ route('insumos.index') }}">
        </x-breadcrumps.wrapper>
    @else
        <x-breadcrumps.wrapper title="Listado de insumos" create="{{ route('insumos.create') }}"
                               ability="insumos" text="Nuevo insumo">
            <x-slot name="afterButtons">
                <x-buttons.reports
                    xlsx="{!! route('insumos.informe', request()->query()) !!}">
                </x-buttons.reports>
            </x-slot>
        </x-breadcrumps.wrapper>
    @endif

    <x-card>
        <x-slot name="title">Insumos</x-slot>
        <x-slot name="description">Lista los insumos para producción.</x-slot>

        <x-searchers.default text="true" dateRange="true"></x-searchers.default>

        <x-table :collection="$data">
            <x-slot name="header">
                <x-th name="nombre">Nombre</x-th>
                <th>Tipo de insumo</th>
                <x-th name="descripcion">Descripción</x-th>
                <x-th name="existencia">Existencia</x-th>
                <x-th name="costo_referencial">Costo referencial</x-th>
                <th>Total</th>
                <x-th name="created_at">Fecha de creación</x-th>
                <th>Acciones</th>
            </x-slot>

            <x-slot name="body">
                @forelse($data as $item)
                    <tr>
                        <td>{{ $item->nombre }}</td>
                        <td>{{ $item->tipo->nombre }}</td>
                        <td>{{ $item->descripcion }}</td>
                        <td>{{ $item->existencia }}</td>
                        <td>{{ $item->costo_referencial }}</td>
                        <td>{{ number_format($item->costo_referencial * $item->existencia, 2) }}</td>
                        <td>{{ $item->created_at->isoFormat('ll') }}</td>
                        <td>
                            <x-buttons.resources :model="$item" ability="insumos"
                                                 edit="insumos.edit"
                                                 destroy="insumos.destroy"
                                                 restore="insumos.restore">
                            </x-buttons.resources>
                        </td>
                    </tr>
                @empty
                    <tr>
                        <th class="text-center" colspan="15">Sin registros</th>
                    </tr>
                @endforelse
            </x-slot>
        </x-table>
    </x-card>
</x-app>

