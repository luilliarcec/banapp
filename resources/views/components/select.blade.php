<div id="field_group_{{ $name }}" class="form-group">
    <label for="field_{{ $name }}" @error($cleanName()) class="text-danger" @enderror>{{ $label }}
        @if ($required)
            <span class="badge badge-info">{{ __('Required') }}</span>
        @endif
    </label>

    <select {{ $attributes->merge(['class' => $styles($errors->has($cleanName()))]) }} name="{{ $name }}"
            id="field_{{ $name }}" {{ $required ? 'required' : '' }} {{ $multiple ? 'multiple' : '' }}>
        @if($empty)
            <option>{{ $empty }}</option>
        @endif

        @if($options)
            @foreach($options as $key => $option)
                <option value="{{ $key }}" {{ $isSelected($key, $errors->any()) }}>{{ $option }}</option>
            @endforeach
        @else
            {{ $slot }}
        @endif
    </select>

    @if($errors->has($cleanName()))
        <div id="{{ $cleanName() }}-feedback" class="invalid-feedback">{{ $errors->first($cleanName()) }}</div>
    @elseif($help)
        <small id="{{ $cleanName() }}-help" class="form-text text-muted">{{ $help }}</small>
    @endif
</div>
