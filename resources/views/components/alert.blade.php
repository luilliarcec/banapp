@props(['message', 'header', 'type'])

<div class="alert alert-{{ $type }}">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>

    <h5 class="text-{{ $type }}">
        @switch($type)
            @case('success')
                <i class="fa fa-check-circle" style="font-size: 14px"></i> {{ $header ?? 'Completado' }}
                @break

            @case('info')
                <i class="fa fa-exclamation-circle" style="font-size: 14px"></i> {{ $header ?? 'Información' }}
                @break

            @case('warning')
                <i class="fa fa-exclamation-triangle" style="font-size: 14px"></i> {{ $header ?? 'Alerta' }}
                @break

            @case('danger')
                <i class="fa fa-times" style="font-size: 14px"></i> {{ $header ?? 'Error' }}
                @break
        @endswitch
    </h5>

    {{ $slot }}
</div>
