<div class="row justify-content-center">
    <div {{ $attributes->merge(['class' => 'grid-margin stretch-card col-12 mt-3']) }}>
        <div class="card">
            @isset($header)
                <div class="card-header">{{ $header }}</div>
            @endisset

            <div class="card-body">
                @isset($title)
                    <h4 class="card-title">{{ $title }}</h4>
                @endisset

                @isset($description)
                    <h6 class="card-subtitle">{{ $description }}</h6>
                @endisset

                {{ $slot }}
            </div>

            @isset($footer)
                <div class="card-footer">{{ $footer }}</div>
            @endisset
        </div>
    </div>
</div>
