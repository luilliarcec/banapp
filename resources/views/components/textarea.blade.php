<div id="field_group_{{ $name }}" class="form-group">
    <label for="field_{{ $name }}" @error($name) class="text-danger" @enderror>{{ $label }}
        @if ($required)
            <span class="badge badge-info">{{ __('Required') }}</span>
        @endif
    </label>

    <textarea {{ $attributes->merge(['class' => $styles($errors->has($name))]) }} name="{{ $name }}"
              id="field_{{ $name }}" {{ $required ? 'required' : '' }}>{{ $value }}</textarea>

    @if($errors->has($name))
        <div id="{{ $name }}-feedback" class="invalid-feedback">{{ $errors->first($name) }}</div>
    @elseif($help)
        <small id="{{ $name }}-help" class="form-text text-muted">{{ $help }}</small>
    @endif
</div>
