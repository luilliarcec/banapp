<form method="post" {{ $attributes }}>
    @csrf
    @method($method)

    {{ $slot }}
</form>
