@props(['print', 'xlsx', 'pdf'])

<div class="btn-group float-right ml-2">
    <button class="btn btn-dark dropdown-toggle dropdown-not-after" data-display="static"
            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        <i class="cil-plus"></i>
    </button>

    <div class="dropdown-menu dropdown-menu-right">
        @isset($print)
            <a href="{{ $print }}" class="dropdown-item" type="button">
                <i class="cil-print mr-2 icon-md"></i> Imprimir
            </a>
        @endisset

        @isset($xlsx)
            <a href="{{ $xlsx }}" class="dropdown-item" type="button">
                <i class="cil-file mr-2 icon-md"></i> Exportar a XLSX
            </a>
        @endisset

        @isset($pdf)
            <a href="{{ $pdf }}" class="dropdown-item" type="button">
                <i class="cil-file mr-2 icon-md"></i> Exportar a PDF
            </a>
        @endisset

        {{ $slot }}
    </div>
</div>
