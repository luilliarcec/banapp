@props(['model', 'ability', 'show', 'edit', 'destroy', 'restore'])

<div class="btn-group" role="group" aria-label="First group">
    @if(isset($restore) and $model->trashed())
        @isset($ability)
            @can('Restaurar ' . $ability)
                <a href="{{ route($restore, $model->getKey()) }}" type="button" class="btn btn-secondary btn-swal"
                   data-title="Restaurar este registro"
                   data-toggle="tooltip" title="Restaurar registro">
                    <i class="cil-action-undo"></i>
                </a>
            @endcan
        @else
            <a href="{{ route($restore, $model->getKey()) }}" type="button" class="btn btn-secondary btn-swal"
               data-title="Restaurar este registro"
               data-toggle="tooltip" title="Restaurar registro">
                <i class="cil-action-undo"></i>
            </a>
        @endisset

        @isset($inactive)
            {{ $inactive }}
        @endisset
    @else
        @isset($show)
            @isset($ability)
                @can('Ver ' . $ability)
                    <a href="{{ route($show, $model->getKey()) }}" type="button" class="btn btn-secondary"
                       data-toggle="tooltip" title="Detalle de registro">
                        <i class="cil-description"></i>
                    </a>
                @endcan
            @else
                <a href="{{ route($show, $model->getKey()) }}" type="button" class="btn btn-secondary"
                   data-toggle="tooltip" title="Detalle de registro">
                    <i class="cil-description"></i>
                </a>
            @endisset
        @endisset

        @isset($edit)
            @isset($ability)
                @can('Editar ' . $ability)
                    <a href="{{ route($edit, $model->getKey()) }}" type="button" class="btn btn-secondary"
                       data-toggle="tooltip" title="Editar registro">
                        <i class="cil-pencil"></i>
                    </a>
                @endcan
            @else
                <a href="{{ route($edit, $model->getKey()) }}" type="button" class="btn btn-secondary"
                   data-toggle="tooltip" title="Editar registro">
                    <i class="cil-pencil"></i>
                </a>
            @endisset
        @endisset

        @isset($destroy)
            @isset($ability)
                @can('Eliminar ' . $ability)
                    <a href="{{ route($destroy, $model->getKey()) }}" type="button" class="btn btn-secondary btn-swal"
                       data-title="Eliminar este registro" data-method="delete"
                       data-toggle="tooltip" title="Eliminar registro">
                        <i class="cil-trash"></i>
                    </a>
                @endcan
            @else
                <a href="{{ route($destroy, $model->getKey()) }}" type="button" class="btn btn-secondary btn-swal"
                   data-title="Eliminar este registro" data-method="delete"
                   data-toggle="tooltip" title="Eliminar registro">
                    <i class="cil-trash"></i>
                </a>
            @endisset
        @endisset

        @isset($active)
            {{ $active }}
        @endisset
    @endif

    @isset($slot)
        {{ $slot }}
    @endisset
</div>
