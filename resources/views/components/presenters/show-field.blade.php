@props(['label', 'classSpan'])

<div class="col-lg-6 col-md-12">
    <div class="form-group row">
        <label {{ $attributes->merge(['class' => 'control-label text-left text-md-right col-md-4']) }}>
            {{ $label }}:
        </label>

        <div class="col">

            {{ $slot }}

            @isset($collection)
                @foreach($collection as $item)
                    <span class="{{ $classSpan ?? 'badge badge-secondary' }}">{{ $item->{$field} }}</span>
                @endforeach
            @endisset
        </div>
    </div>
</div>
