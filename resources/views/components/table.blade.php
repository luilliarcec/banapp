<div class="{{ $responsiveStyles }}">
    <table {{ $attributes->merge(['class' => 'table table-bordered table-striped table-hover mt-4']) }}>
        @isset($header)
            <thead>
            <tr>
                {{ $header }}
            </tr>
            </thead>
        @endisset

        @isset($body)
            <tbody>
                {{ $body }}
            </tbody>
        @endisset
    </table>
</div>

@if(method_exists($collection, 'links'))
    <div class="mt-5">
        {{ $collection->links() }}
    </div>
@endif
