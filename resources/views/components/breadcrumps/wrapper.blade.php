@props(['title', 'back', 'ability', 'create', 'modal', 'text'])

<div class="row page-titles">
    <div class="col-12 col-md-6 align-self-center">
        <h3 class="text-themecolor mb-0 mt-0">{{ $title }}</h3>
        @isset($slot)
            <ol class="breadcrumb">
                {{ $slot }}
            </ol>
        @endisset
    </div>

    <div class="col-12 col-md-6 align-self-center">
        @isset($back)
            <a href="{{ $back }}"
               class="btn btn-dark float-right">
                <i class="cil-arrow-left"></i>
                Atrás
            </a>
        @endisset

        {{ $afterButtons ?? '' }}

        @if(isset($create))
            @isset($ability)
                @can('Crear ' . $ability)
                    <a href="{{ $create }}" class="btn btn-success float-right">
                        <i class="cil-plus"></i>
                        {{ $text ?? 'Crear registro' }}
                    </a>
                @endcan
            @else
                <a href="{{ $create }}" class="btn btn-success float-right">
                    <i class="cil-plus"></i>
                    {{ $text ?? 'Crear registro' }}
                </a>
            @endisset
        @elseif(isset($modal))
            @isset($ability)
                @can('Crear ' . $ability)
                    <button type="button" class="btn btn-success float-right" data-toggle="modal"
                            data-target="{{ $modal ?? '#form-modal' }}">
                        <i class="cil-plus"></i>
                        {{ $text ?? 'Crear registro' }}
                    </button>
                @endcan
            @else
                <button type="button" class="btn btn-success float-right" data-toggle="modal"
                        data-target="{{ $modal ?? '#form-modal' }}">
                    <i class="cil-plus"></i>
                    {{ $text ?? 'Crear registro' }}
                </button>
            @endisset
        @endif

        {{ $beforeButtons ?? '' }}
    </div>
</div>
