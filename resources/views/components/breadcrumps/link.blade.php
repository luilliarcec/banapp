@props(['link'])

<li {{ $attributes->merge(['class' => 'breadcrumb-item']) }}>
    @isset($link)
        <a href="{{ $link }}">{{ $slot }}</a>
    @else
        {{ $slot }}
    @endisset
</li>
