<div class="col-12 col-md-6">
    <div class="form-group row align-items-center justify-content-end">
        <div class="col-12 col-md-5">
            <input id="desde" type="date" class="form-control" name="desde" aria-label="Desde"
                   value="{{ request('desde') }}">
        </div>

        <div class="col-12 col-md-1 text-center py-2 p-md-0">
            <span class="badge badge-info">hasta</span>
        </div>

        <div class="col-12 col-md-5">
            <input id="hasta" type="date" class="form-control" name="hasta" aria-label="Hasta"
                   value="{{ request('hasta') }}">
        </div>
    </div>
</div>
