@props(['name', 'options', 'default'])

<div class="col-12 col-md-6">
    <div class="form-group row align-items-center justify-content-end">
        <div class="col-12 col-md-4">
            <select id="{{ $name ?? 'options_filter' }}" name="{{ $name ?? 'options' }}"
                    class="form-control" aria-label="">

                {{ $slot }}

                @foreach($options as $key => $value)
                    @if (request($name ?? 'options') == $key)
                        <option value="{{ $key }}" selected>
                            {{ $value }}
                        </option>
                    @else
                        @if (! request()->has($name ?? 'options') && $key == ($default ?? null))
                            <option value="{{ $key }}" selected>
                                {{ $value }}
                            </option>
                        @else
                            <option value="{{ $key }}">
                                {{ $value }}
                            </option>
                        @endif
                    @endif
                @endforeach
            </select>
        </div>
    </div>
</div>
