<div class="col-12 col-md-4">
    <div class="input-group">
        <input type="text" name="search" class="form-control" placeholder="Buscar por..."
               aria-label="Buscar" value="{{ request('search') }}">
        <div class="input-group-append">
            <button class="btn btn-outline-secondary" type="submit">
                <i class="cil-search"></i>
            </button>
        </div>
    </div>
</div>
