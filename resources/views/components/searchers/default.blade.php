@props(['dateRange', 'text', 'route'])

<form class="app-search" action="{{ $route ?? '' }}">
    <div class="row justify-content-end">
        {{ $slot }}

        @isset($dateRange)
            @include('components.searchers.date-range')
        @endisset

        @isset($text)
            @include('components.searchers.text')
        @else
            <div class="col-12 col-md-2">
                <button class="btn btn-outline-secondary w-100" type="submit">
                    Filtrar <i class="cil-search"></i>
                </button>
            </div>
        @endisset
    </div>
</form>
