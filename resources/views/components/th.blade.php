<th>
    <a href="{{ $links }}" class="link-sortable">
        {{ $slot }} <i class="{{ $styles }}"></i>
    </a>
</th>
