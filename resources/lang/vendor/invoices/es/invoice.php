<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Invoice Language Lines
    |--------------------------------------------------------------------------
    */

    'serial'          => 'No.',
    'date'            => 'Fecha de factura',
    'seller'          => 'Vendedor',
    'buyer'           => 'Comprador',
    'address'         => 'Dirección',
    'code'            => 'Código',
    'vat'             => 'IVA código',
    'phone'           => 'Teléfono',
    'description'     => 'Descripción',
    'units'           => 'Unidades',
    'quantity'        => 'Cantidad',
    'price'           => 'Precio',
    'discount'        => 'Descuento',
    'tax'             => 'Impuesto',
    'sub_total'       => 'Subtotal',
    'total_discount'  => 'Total descuento',
    'taxable_amount'  => 'Subtotal',
    'total_taxes'     => 'Total de impuestos',
    'tax_rate'        => 'Porcentaje de IVA',
    'total_amount'    => 'Total a pagar',
    'pay_until'       => 'Porfavor pagar hasta',
    'amount_in_words' => 'Cantidad en palabras',
    'notes'           => 'Notas',
    'shipping'        => 'Envío',

];
