<?php

use App\Http\Controllers\ClienteController;
use App\Http\Controllers\EmpresaController;
use App\Http\Controllers\InsumoController;
use App\Http\Controllers\LoteController;
use App\Http\Controllers\ParametroController;
use App\Http\Controllers\ProduccionController;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\TipoController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\VariedadController;
use App\Http\Controllers\VentaController;
use Illuminate\Support\Facades\Route;

/** Home APP */
Route::view('/', 'layouts.app')->name('app.home');

/** Empresa */
Route::get('empresa', [EmpresaController::class, 'show'])->name('empresas.show');
Route::get('empresa/actualizar', [EmpresaController::class, 'edit'])->name('empresas.edit');
Route::put('empresa/actualizar', [EmpresaController::class, 'update'])->name('empresas.update');

/** Parámetros */
Route::get('parametros', [ParametroController::class, 'show'])->name('parametros.show');
Route::get('parametros/actualizar', [ParametroController::class, 'edit'])->name('parametros.edit');
Route::put('parametros/actualizar', [ParametroController::class, 'update'])->name('parametros.update');

/** Roles */
Route::resource('roles', RoleController::class);

/** Tipos de insumos */
Route::resource('tipos-insumo', TipoController::class)->except(['destroy', 'show'])
    ->parameters(['tipos-insumo' => 'tipo']);

/** Lotes */
Route::get('lotes/eliminados', [LoteController::class, 'index'])->name('lotes.trash');
Route::put('lotes/{lote}/restaurar', [LoteController::class, 'restore'])->name('lotes.restore');
Route::resource('lotes', LoteController::class)->except(['show']);

/** Variedades */
Route::get('variedades/eliminados', [VariedadController::class, 'index'])->name('variedades.trash');
Route::put('variedades/{variedad}/restaurar', [VariedadController::class, 'restore'])->name('variedades.restore');
Route::resource('variedades', VariedadController::class)->except(['show'])
    ->parameters(['variedades' => 'variedad']);

/** Usuarios */
Route::get('usuarios/eliminados', [UserController::class, 'index'])->name('usuarios.trash');
Route::put('usuarios/{usuario}/restaurar', [UserController::class, 'restore'])->name('usuarios.restore');
Route::resource('usuarios', UserController::class);

/** Clientes */
Route::get('clientes/informe', [ClienteController::class, 'informe'])->name('clientes.informe');
Route::get('clientes/eliminados', [ClienteController::class, 'index'])->name('clientes.trash');
Route::put('clientes/{cliente}/restaurar', [ClienteController::class, 'restore'])->name('clientes.restore');
Route::resource('clientes', ClienteController::class)->except('show');

/** Insumos */
Route::get('insumos/informe', [InsumoController::class, 'informe'])->name('insumos.informe');
Route::get('insumos/eliminados', [InsumoController::class, 'index'])->name('insumos.trash');
Route::put('insumos/{insumo}/restaurar', [InsumoController::class, 'restore'])->name('insumos.restore');
Route::resource('insumos', InsumoController::class)->except(['show']);

/** Produccion */
Route::get('produccion/informe', [ProduccionController::class, 'informe'])->name('produccion.informe');
Route::resource('produccion', ProduccionController::class)->only('index', 'create', 'store', 'show');

/** Venta */
Route::get('ventas/informe', [VentaController::class, 'informe'])->name('ventas.informe');
Route::get('ventas/{venta}/factura', [VentaController::class, 'factura'])->name('ventas.factura');
Route::resource('ventas', VentaController::class)->only('index', 'create', 'store', 'show');
