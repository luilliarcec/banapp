/**
 * Logout
 */
document.querySelectorAll('.btn-logout').forEach((element) => {
    element.addEventListener('click', (event) => {
        event.preventDefault();
        document.getElementById('form-logout').submit();
    });
});

/**
 * Acciones asíncronas con botones
 *
 * @type {NodeListOf<Element>}
 */
const btnSwal = document.querySelectorAll('.btn-swal');

btnSwal.forEach(btn => {
    btn.addEventListener('click', function (e) {
        e.preventDefault();

        let options = {
            method: `${this.dataset.method ?? 'put'}`,
            url: `${this.href}`,
        };

        Swal.fire({
            title: '¿Está usted seguro?',
            text: `¡${this.dataset.title ?? this.dataset.originalTitle}!`,
            icon: `${this.dataset.method === 'delete' ? 'warning' : this.dataset.color ?? 'info'}`,
            confirmButtonColor: `${this.dataset.method === 'delete' ? '#DD6B55' : ''}`,
            showCancelButton: true,
            confirmButtonText: '¡Sí, continuar!',
            cancelButtonText: '¡No, cancelar!',
            reverseButtons: true,
            showLoaderOnConfirm: true,
            preConfirm: () => {
                return (
                    axios(options)
                        .then(response => response.data)
                        .catch(error => {
                            if (error.response.data.message) {
                                Swal.showValidationMessage(`Surgió un error: ${error.response.data.message}`)
                            } else {
                                Swal.showValidationMessage(`Surgió un error: ${error.response.statusText} ${error.response.status}`)
                            }
                        })
                );
            },
            allowOutsideClick: () => !Swal.isLoading()
        }).then(result => {
            if (result.value) {
                Swal.fire({
                    title: '¡Acción completada correctamente!',
                    text: result.value.message,
                    icon: 'success',
                    preConfirm: () => {
                        return result.value;
                    }
                }).then((result) => {
                    if (result.value.redirectTo)
                        location = result.value.redirectTo;
                    else
                        location = location.href
                })
            }
        })
    })
});

/**
 * Valida que no contenga dígitos
 *
 * @param event
 * @returns {boolean}
 */
function alpha(event) {
    let key = (document.all) ? event.keyCode : event.which;

    let pattern = /\D/;

    let letter = String.fromCharCode(key);

    return pattern.test(letter);
}

/**
 * Valida dígitos con formato de entero
 *
 * @param event
 * @returns {boolean}
 */
function digits(event) {
    let key = (document.all) ? event.keyCode : event.which;

    let pattern = /\d/;

    let digit = String.fromCharCode(key);

    return pattern.test(digit);
}

/**
 * Valida dígitos con formato de decimal
 *
 * @param event
 * @returns {boolean}
 */
function decimals(event) {
    let key = (document.all) ? event.keyCode : event.which;

    let pattern = /^\d*\.?\d*$/;

    let digit = String.fromCharCode(key);

    return pattern.test(digit);
}
